# Fina i brza hrana

### Description
Online food delivery service "Fina i brza hrana".

### Team Members
* Antonio Junakovic
* Mihael Jaksic
* Fran Hrsak
* Ivan Glavinovic
* Kristijan Boskovic
* Marta Grotic 

### Website URL
https://finaibrzahrana.azurewebsites.net/
