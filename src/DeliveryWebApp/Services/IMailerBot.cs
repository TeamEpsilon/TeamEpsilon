﻿using DeliveryWebApp.Database.Entries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp.Services
{
    public interface IMailerBot
    {
        /// <summary>
        /// Sends verification code to email.
        /// </summary>
        /// <param name="email">Target email.</param>
        /// <param name="code">Verification code.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> SendVerificationCode(string email, string code);

        /// <summary>
        /// Sends email change verification code.
        /// </summary>
        /// <param name="email">Target's new email.</param>
        /// <param name="username">Target's username.</param>
        /// <param name="code">Verification code.</param>
        /// <param name="requestId">Change request ID.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> SendEmailChangeCode(string email, string username, string code, Guid requestId);

        /// <summary>
        /// Sends deleted restaurant mail.
        /// </summary>
        /// <param name="email">Target email.</param>
        /// <param name="restaurantName">Deleted restaurant name.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> SendDeletedRestaurant(string email, string restaurantName);

        /// <summary>
        /// Sends order details to the account email of the restaurant.
        /// </summary>
        /// <param name="email">Restaurant owner account email.</param>
        /// <param name="restaurantName">Restaurant name.</param>
        /// <param name="takeoutItems">Takeout items list (include meals).</param>
        /// <param name="takeout">Takeout object.</param>
        /// <param name="whoOrdered">Person who ordered.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> SendOrderDetails(string email, string restaurantName, IEnumerable<TakeoutItem> takeoutItems, Takeout takeout, Account whoOrdered);
    }
}
