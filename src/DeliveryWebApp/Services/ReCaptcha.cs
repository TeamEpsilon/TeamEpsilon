﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace DeliveryWebApp.Services
{
    public class ReCaptcha : IReCaptcha
    {
        private IHttpClientFactory _httpClientFactory;
        private IConfiguration _configuration;

        /// <summary>
        /// Create ReCaptcha service.
        /// </summary>
        /// <param name="httpClientFactory">HttpClientFactory dependency.</param>
        /// <param name="configuration">Configuration dependency</param>
        public ReCaptcha(IHttpClientFactory httpClientFactory, IConfiguration configuration)
        {
            _httpClientFactory = httpClientFactory;
            _configuration = configuration;
        }

        public async Task<bool> IsReCaptchaResponseValidAsync(string reCaptchaResponse)
        {
            // Construct POST form data dictionary
            Dictionary<string, string> query = new Dictionary<string, string>
            {
                { "secret", _configuration["ReCaptcha:SecretKey"] },
                { "response", reCaptchaResponse }
            };

            // POST on Google servers and wait for response
            HttpResponseMessage response = await _httpClientFactory.CreateClient().PostAsync("https://www.google.com/recaptcha/api/siteverify", new FormUrlEncodedContent(query));

            // Read response to end and parse as JSON
            JObject responseMessage = JObject.Parse(await response.Content.ReadAsStringAsync());

            // Return true if it succeeded or false otherwise
            return responseMessage["success"].ToObject<bool>();
        }
    }
}
