﻿using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Threading.Tasks;

namespace DeliveryWebApp.Services
{
    public class Storage : IStorage
    {
        private IConfiguration _configuration;
        private CloudStorageAccount _storageAccount;

        /// <summary>
        /// Create ReCaptcha service.
        /// </summary>
        /// <param name="configuration">Configuration dependency.</param>
        public Storage(IConfiguration configuration)
        {
            _configuration = configuration;
            _storageAccount = new CloudStorageAccount(new StorageCredentials(_configuration["AzureStorage:Account"], _configuration["AzureStorage:Key"]), true);
        }
        
        public async Task<string> Upload(string containerName, string fileName, byte[] data)
        {
            try
            {
                CloudBlobClient blobClient = _storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(containerName);
                await container.CreateIfNotExistsAsync(BlobContainerPublicAccessType.Blob, new BlobRequestOptions(), new OperationContext());
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
                await blockBlob.UploadFromByteArrayAsync(data, 0, data.Length);
                return blockBlob.Uri.ToString();
            }
            catch
            {
            }
            return null;
        }
    }
}
