﻿using DeliveryWebApp.Database.Entries;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;

namespace DeliveryWebApp.Services
{
    public class MailerBot : IMailerBot
    {
        private ISendGridClient _sendGridClient;

        public MailerBot(ISendGridClient sendGridClient)
        {
            _sendGridClient = sendGridClient;
        }

        public async Task<bool> SendVerificationCode(string email, string code)
        {
            try
            {
                SendGridMessage mail = MailHelper.CreateSingleEmail(
                    new EmailAddress(Info.MailerBotEmail),
                    new EmailAddress(email), "Potvrda računa",
                    "Zahvaljujemo na registraciji.\nPoveznica za aktivaciju računa: " + Info.Website + "Confirm?email=" + Uri.EscapeUriString(email) + "&code=" + Uri.EscapeUriString(code) + "\nVaš verifikacijski kod je: " + code,
                    "Zahvaljujemo na registraciji.<br>Poveznica za aktivaciju računa: <a href=\"" + Info.Website + "Confirm?email=" + Uri.EscapeUriString(email) + "&code=" + Uri.EscapeUriString(code) + "\">Aktivacija računa</a><br>Vaš verifikacijski kod je: <strong>" + code + "</strong>"
                    );
                await _sendGridClient.SendEmailAsync(mail);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public async Task<bool> SendEmailChangeCode(string email, string username, string code, Guid requestId)
        {
            try
            {
                SendGridMessage mail = MailHelper.CreateSingleEmail(
                    new EmailAddress(Info.MailerBotEmail),
                    new EmailAddress(email), "Promjena adrese e-pošte",
                    "Korisnički profil " + username + " zahtjeva promjenu adrese e-pošte.\nPoveznica za potvrdu promjene: " + Info.Website + "Account/Confirm?id=" + Uri.EscapeUriString(requestId.ToString()) + "&code=" + Uri.EscapeUriString(code) + "\nVaš verifikacijski kod je: " + code,
                    "Korisnički profil <strong>" + username + "</strong> zahtjeva promjenu adrese e-pošte.<br>Poveznica za potvrdu promjene: <a href=\"" + Info.Website + "Account/Confirm?id=" + Uri.EscapeUriString(requestId.ToString()) + "&code=" + Uri.EscapeUriString(code) + "\">Aktivacija računa</a><br>Vaš verifikacijski kod je: <strong>" + code + "</strong>"
                    );
                await _sendGridClient.SendEmailAsync(mail);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public async Task<bool> SendDeletedRestaurant(string email, string restaurantName)
        {
            try
            {
                SendGridMessage mail = MailHelper.CreateSingleEmail(
                    new EmailAddress(Info.MailerBotEmail),
                    new EmailAddress(email), "[VAŽNO] Obrisan restoran",
                    "Poštovani\n\nVaš restoran " + restaurantName + " je obrisan iz sustava Fina i brza hrana od strane naših administratora.\nAko smatrate da smo pogriješili, kontaktirajte nas na službeni email.\n\nLijep pozdrav,\nFina i brza hrana (mailerbot)",
                    "Poštovani<br><br>Vaš restoran <strong>" + restaurantName + "</strong> je obrisan iz sustava <i>Fina i brza hrana</i> od strane naših administratora.<br>Ako smatrate da smo pogriješili, kontaktirajte nas na službeni email.<br><br>Lijep pozdrav,<br>Fina i brza hrana (mailerbot)"
                    );
                await _sendGridClient.SendEmailAsync(mail);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public async Task<bool> SendOrderDetails(string email, string restaurantName, IEnumerable<TakeoutItem> takeoutItems, Takeout takeout, Account whoOrdered)
        {
            try
            {
                string raw = "Poštovani,\n\nVaš restoran " + restaurantName + " je zaprimio novu narudžbu:\n";
                string html = "Poštovani,<br><br>Vaš restoran <strong>" + restaurantName + "</strong> je zaprimio novu narudžbu:<br>";

                BigInteger total = 0;
                string strprice = "";
                foreach (TakeoutItem tki in takeoutItems)
                {
                    BigInteger nowprice = tki.Quantity * tki.TargetMeal.Price;
                    total += nowprice;
                    strprice = nowprice.ToString();
                    strprice = strprice.Insert(strprice.Length - 2, ".");
                    raw += "\n- " + tki.TargetMeal.Name + ", " + tki.Quantity.ToString() + " kom., ukupno " + strprice + " HRK";
                    html += "<br>- " + tki.TargetMeal.Name + ", " + tki.Quantity.ToString() + " kom., ukupno " + strprice + " HRK";
                    if ((tki.Comment != null) && (tki.Comment.Trim().Length > 0))
                    {
                        raw += "   [Komentar: " + tki.Comment + "]";
                        html += "   [Komentar: <i>" + tki.Comment + "</i>]";
                    }
                }

                strprice = total.ToString();
                strprice = strprice.Insert(strprice.Length - 2, ".");
                raw += "\n\nUkupan iznos: " + strprice + " HRK\n\nDostaviti za:\n\n" + whoOrdered.FirstName + " " + whoOrdered.LastName + "\n" + takeout.Address + "\n+385 " + whoOrdered.CellphoneNumber + "\n\nLijep pozdrav,\nFina i brza hrana (mailerbot)";
                html += "<br><br>Ukupan iznos: <strong>" + strprice + " HRK</strong><br><br>Dostaviti za:<br><br>" + whoOrdered.FirstName + " " + whoOrdered.LastName + "<br>" + takeout.Address + "<br>+385 " + whoOrdered.CellphoneNumber + "<br><br>Lijep pozdrav,<br>Fina i brza hrana (mailerbot)";

                SendGridMessage mail = MailHelper.CreateSingleEmail(
                    new EmailAddress(Info.MailerBotEmail),
                    new EmailAddress(email), "Nova narudžba za restoran " + restaurantName,
                    raw,
                    html
                    );
                await _sendGridClient.SendEmailAsync(mail);
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
