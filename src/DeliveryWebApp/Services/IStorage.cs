﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp.Services
{
    public interface IStorage
    {
        /// <summary>
        /// Uploads data to storage and retrieves the link.
        /// </summary>
        /// <param name="containerName">Container name.</param>
        /// <param name="fileName">File name in container.</param>
        /// <param name="data">Data to upload.</param>
        /// <returns>Link to data.</returns>
        Task<string> Upload(string containerName, string fileName, byte[] data);
    }
}
