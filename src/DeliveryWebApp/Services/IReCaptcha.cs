﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp.Services
{
    public interface IReCaptcha
    {
        /// <summary>
        /// Check validity of ReCaptcha response.
        /// </summary>
        /// <param name="reCaptchaResponse">ReCaptcha response.</param>
        /// <returns>True if response is valid, false otherwise.</returns>
        Task<bool> IsReCaptchaResponseValidAsync(string reCaptchaResponse);
    }
}
