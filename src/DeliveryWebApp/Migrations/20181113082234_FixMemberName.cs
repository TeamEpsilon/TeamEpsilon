﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeliveryWebApp.Migrations
{
    public partial class FixMemberName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TakeoutItem_Accounts_CreatedByIDID",
                table: "TakeoutItem");

            migrationBuilder.RenameColumn(
                name: "CreatedByIDID",
                table: "TakeoutItem",
                newName: "CreatedByID");

            migrationBuilder.RenameIndex(
                name: "IX_TakeoutItem_CreatedByIDID",
                table: "TakeoutItem",
                newName: "IX_TakeoutItem_CreatedByID");

            migrationBuilder.AddForeignKey(
                name: "FK_TakeoutItem_Accounts_CreatedByID",
                table: "TakeoutItem",
                column: "CreatedByID",
                principalTable: "Accounts",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TakeoutItem_Accounts_CreatedByID",
                table: "TakeoutItem");

            migrationBuilder.RenameColumn(
                name: "CreatedByID",
                table: "TakeoutItem",
                newName: "CreatedByIDID");

            migrationBuilder.RenameIndex(
                name: "IX_TakeoutItem_CreatedByID",
                table: "TakeoutItem",
                newName: "IX_TakeoutItem_CreatedByIDID");

            migrationBuilder.AddForeignKey(
                name: "FK_TakeoutItem_Accounts_CreatedByIDID",
                table: "TakeoutItem",
                column: "CreatedByIDID",
                principalTable: "Accounts",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
