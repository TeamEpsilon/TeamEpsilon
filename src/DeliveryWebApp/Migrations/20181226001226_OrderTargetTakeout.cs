﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeliveryWebApp.Migrations
{
    public partial class OrderTargetTakeout : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_TakeoutItems_TargetedItemID",
                table: "Orders");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Takeouts_TargetedItemID",
                table: "Orders",
                column: "TargetedItemID",
                principalTable: "Takeouts",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Takeouts_TargetedItemID",
                table: "Orders");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_TakeoutItems_TargetedItemID",
                table: "Orders",
                column: "TargetedItemID",
                principalTable: "TakeoutItems",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
