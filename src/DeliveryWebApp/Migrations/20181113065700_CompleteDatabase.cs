﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DeliveryWebApp.Migrations
{
    public partial class CompleteDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Meals",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    ServedAtID = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Price = table.Column<long>(nullable: false),
                    PictureURL = table.Column<string>(nullable: true),
                    PreparationMinutes = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Meals", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Meals_Restaurants_ServedAtID",
                        column: x => x.ServedAtID,
                        principalTable: "Restaurants",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Takeouts",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    PlacedByID = table.Column<Guid>(nullable: true),
                    MapLongitude = table.Column<double>(nullable: false),
                    MapLatitude = table.Column<double>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    PlacedTime = table.Column<DateTime>(nullable: false),
                    IsDelivered = table.Column<bool>(nullable: false),
                    DeliveredTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Takeouts", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Takeouts_Accounts_PlacedByID",
                        column: x => x.PlacedByID,
                        principalTable: "Accounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TakeoutItem",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CreatedByID = table.Column<Guid>(nullable: true),
                    TargetMealID = table.Column<Guid>(nullable: true),
                    TargetTakeoutID = table.Column<Guid>(nullable: true),
                    Quantity = table.Column<long>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    AccountID = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TakeoutItem", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TakeoutItem_Accounts_AccountID",
                        column: x => x.AccountID,
                        principalTable: "Accounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TakeoutItem_TakeoutItem_CreatedByID",
                        column: x => x.CreatedByID,
                        principalTable: "TakeoutItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TakeoutItem_Meals_TargetMealID",
                        column: x => x.TargetMealID,
                        principalTable: "Meals",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TakeoutItem_Takeouts_TargetTakeoutID",
                        column: x => x.TargetTakeoutID,
                        principalTable: "Takeouts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    AssigneeID = table.Column<Guid>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    MapLatitude = table.Column<double>(nullable: false),
                    MapLongitude = table.Column<double>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    TargetedItemID = table.Column<Guid>(nullable: true),
                    PlacedTime = table.Column<DateTime>(nullable: false),
                    IsCompleted = table.Column<bool>(nullable: false),
                    CompletedTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Orders_Accounts_AssigneeID",
                        column: x => x.AssigneeID,
                        principalTable: "Accounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_TakeoutItem_TargetedItemID",
                        column: x => x.TargetedItemID,
                        principalTable: "TakeoutItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Meals_ServedAtID",
                table: "Meals",
                column: "ServedAtID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_AssigneeID",
                table: "Orders",
                column: "AssigneeID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_TargetedItemID",
                table: "Orders",
                column: "TargetedItemID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_PlacedTime_ID",
                table: "Orders",
                columns: new[] { "PlacedTime", "ID" });

            migrationBuilder.CreateIndex(
                name: "IX_TakeoutItem_AccountID",
                table: "TakeoutItem",
                column: "AccountID");

            migrationBuilder.CreateIndex(
                name: "IX_TakeoutItem_CreatedByID",
                table: "TakeoutItem",
                column: "CreatedByID");

            migrationBuilder.CreateIndex(
                name: "IX_TakeoutItem_TargetMealID",
                table: "TakeoutItem",
                column: "TargetMealID");

            migrationBuilder.CreateIndex(
                name: "IX_TakeoutItem_TargetTakeoutID",
                table: "TakeoutItem",
                column: "TargetTakeoutID");

            migrationBuilder.CreateIndex(
                name: "IX_Takeouts_PlacedByID",
                table: "Takeouts",
                column: "PlacedByID");

            migrationBuilder.CreateIndex(
                name: "IX_Takeouts_PlacedTime_ID",
                table: "Takeouts",
                columns: new[] { "PlacedTime", "ID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "TakeoutItem");

            migrationBuilder.DropTable(
                name: "Meals");

            migrationBuilder.DropTable(
                name: "Takeouts");
        }
    }
}
