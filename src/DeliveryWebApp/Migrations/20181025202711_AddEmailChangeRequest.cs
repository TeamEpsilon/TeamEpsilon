﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DeliveryWebApp.Migrations
{
    public partial class AddEmailChangeRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EmailChangeRequests",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    TargetAccount = table.Column<Guid>(nullable: false),
                    NewEmail = table.Column<string>(nullable: true),
                    VerificationCode = table.Column<string>(nullable: true),
                    Expires = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailChangeRequests", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmailChangeRequests_TargetAccount",
                table: "EmailChangeRequests",
                column: "TargetAccount");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmailChangeRequests");
        }
    }
}
