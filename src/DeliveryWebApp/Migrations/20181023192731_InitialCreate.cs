﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DeliveryWebApp.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccessTokens",
                columns: table => new
                {
                    AccessID = table.Column<Guid>(nullable: false),
                    AccessKey = table.Column<Guid>(nullable: false),
                    AccountID = table.Column<Guid>(nullable: false),
                    Expiration = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessTokens", x => x.AccessID);
                });

            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Username = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PasswordHashed = table.Column<byte[]>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    CellphoneNumber = table.Column<string>(nullable: true),
                    ProfilePictureURL = table.Column<string>(nullable: true),
                    VerificationCode = table.Column<string>(nullable: true),
                    IsVerified = table.Column<bool>(nullable: false),
                    RemainingVerificationTries = table.Column<int>(nullable: false),
                    RemainingVerificationResends = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccessTokens_AccountID",
                table: "AccessTokens",
                column: "AccountID");

            migrationBuilder.CreateIndex(
                name: "IX_AccessTokens_AccessID_AccessKey",
                table: "AccessTokens",
                columns: new[] { "AccessID", "AccessKey" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_Email",
                table: "Accounts",
                column: "Email",
                unique: true,
                filter: "[Email] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_Username",
                table: "Accounts",
                column: "Username",
                unique: true,
                filter: "[Username] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccessTokens");

            migrationBuilder.DropTable(
                name: "Accounts");
        }
    }
}
