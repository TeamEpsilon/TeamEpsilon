﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeliveryWebApp.Migrations
{
    public partial class AddSpecialUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSpecial",
                table: "Accounts",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSpecial",
                table: "Accounts");
        }
    }
}
