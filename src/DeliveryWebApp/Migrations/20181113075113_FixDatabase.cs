﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DeliveryWebApp.Migrations
{
    public partial class FixDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TakeoutItem_Accounts_AccountID",
                table: "TakeoutItem");

            migrationBuilder.DropForeignKey(
                name: "FK_TakeoutItem_TakeoutItem_CreatedByID",
                table: "TakeoutItem");

            migrationBuilder.DropIndex(
                name: "IX_TakeoutItem_AccountID",
                table: "TakeoutItem");

            migrationBuilder.DropColumn(
                name: "AccountID",
                table: "TakeoutItem");

            migrationBuilder.RenameColumn(
                name: "CreatedByID",
                table: "TakeoutItem",
                newName: "CreatedByIDID");

            migrationBuilder.RenameIndex(
                name: "IX_TakeoutItem_CreatedByID",
                table: "TakeoutItem",
                newName: "IX_TakeoutItem_CreatedByIDID");

            migrationBuilder.AddForeignKey(
                name: "FK_TakeoutItem_Accounts_CreatedByIDID",
                table: "TakeoutItem",
                column: "CreatedByIDID",
                principalTable: "Accounts",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TakeoutItem_Accounts_CreatedByIDID",
                table: "TakeoutItem");

            migrationBuilder.RenameColumn(
                name: "CreatedByIDID",
                table: "TakeoutItem",
                newName: "CreatedByID");

            migrationBuilder.RenameIndex(
                name: "IX_TakeoutItem_CreatedByIDID",
                table: "TakeoutItem",
                newName: "IX_TakeoutItem_CreatedByID");

            migrationBuilder.AddColumn<Guid>(
                name: "AccountID",
                table: "TakeoutItem",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TakeoutItem_AccountID",
                table: "TakeoutItem",
                column: "AccountID");

            migrationBuilder.AddForeignKey(
                name: "FK_TakeoutItem_Accounts_AccountID",
                table: "TakeoutItem",
                column: "AccountID",
                principalTable: "Accounts",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TakeoutItem_TakeoutItem_CreatedByID",
                table: "TakeoutItem",
                column: "CreatedByID",
                principalTable: "TakeoutItem",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
