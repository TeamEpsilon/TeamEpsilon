﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeliveryWebApp.Migrations
{
    public partial class FixTakeoutItems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_TakeoutItem_TargetedItemID",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_TakeoutItem_Accounts_CreatedByID",
                table: "TakeoutItem");

            migrationBuilder.DropForeignKey(
                name: "FK_TakeoutItem_Meals_TargetMealID",
                table: "TakeoutItem");

            migrationBuilder.DropForeignKey(
                name: "FK_TakeoutItem_Takeouts_TargetTakeoutID",
                table: "TakeoutItem");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TakeoutItem",
                table: "TakeoutItem");

            migrationBuilder.RenameTable(
                name: "TakeoutItem",
                newName: "TakeoutItems");

            migrationBuilder.RenameIndex(
                name: "IX_TakeoutItem_TargetTakeoutID",
                table: "TakeoutItems",
                newName: "IX_TakeoutItems_TargetTakeoutID");

            migrationBuilder.RenameIndex(
                name: "IX_TakeoutItem_TargetMealID",
                table: "TakeoutItems",
                newName: "IX_TakeoutItems_TargetMealID");

            migrationBuilder.RenameIndex(
                name: "IX_TakeoutItem_CreatedByID",
                table: "TakeoutItems",
                newName: "IX_TakeoutItems_CreatedByID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TakeoutItems",
                table: "TakeoutItems",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_TakeoutItems_TargetedItemID",
                table: "Orders",
                column: "TargetedItemID",
                principalTable: "TakeoutItems",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TakeoutItems_Accounts_CreatedByID",
                table: "TakeoutItems",
                column: "CreatedByID",
                principalTable: "Accounts",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TakeoutItems_Meals_TargetMealID",
                table: "TakeoutItems",
                column: "TargetMealID",
                principalTable: "Meals",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TakeoutItems_Takeouts_TargetTakeoutID",
                table: "TakeoutItems",
                column: "TargetTakeoutID",
                principalTable: "Takeouts",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_TakeoutItems_TargetedItemID",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_TakeoutItems_Accounts_CreatedByID",
                table: "TakeoutItems");

            migrationBuilder.DropForeignKey(
                name: "FK_TakeoutItems_Meals_TargetMealID",
                table: "TakeoutItems");

            migrationBuilder.DropForeignKey(
                name: "FK_TakeoutItems_Takeouts_TargetTakeoutID",
                table: "TakeoutItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TakeoutItems",
                table: "TakeoutItems");

            migrationBuilder.RenameTable(
                name: "TakeoutItems",
                newName: "TakeoutItem");

            migrationBuilder.RenameIndex(
                name: "IX_TakeoutItems_TargetTakeoutID",
                table: "TakeoutItem",
                newName: "IX_TakeoutItem_TargetTakeoutID");

            migrationBuilder.RenameIndex(
                name: "IX_TakeoutItems_TargetMealID",
                table: "TakeoutItem",
                newName: "IX_TakeoutItem_TargetMealID");

            migrationBuilder.RenameIndex(
                name: "IX_TakeoutItems_CreatedByID",
                table: "TakeoutItem",
                newName: "IX_TakeoutItem_CreatedByID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TakeoutItem",
                table: "TakeoutItem",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_TakeoutItem_TargetedItemID",
                table: "Orders",
                column: "TargetedItemID",
                principalTable: "TakeoutItem",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TakeoutItem_Accounts_CreatedByID",
                table: "TakeoutItem",
                column: "CreatedByID",
                principalTable: "Accounts",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TakeoutItem_Meals_TargetMealID",
                table: "TakeoutItem",
                column: "TargetMealID",
                principalTable: "Meals",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TakeoutItem_Takeouts_TargetTakeoutID",
                table: "TakeoutItem",
                column: "TargetTakeoutID",
                principalTable: "Takeouts",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
