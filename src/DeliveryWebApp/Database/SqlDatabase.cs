﻿using DeliveryWebApp.Database.Entries;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace DeliveryWebApp.Database
{
    public class SqlDatabase : DbContext
    {
        /// <summary>
        /// An internal copy of connection string.
        /// </summary>
        public static string ConnectionString { private get; set; }

        /// <summary>
        /// Table set of all accounts.
        /// </summary>
        public DbSet<Account> Accounts { get; set; }

        /// <summary>
        /// Table set of all access tokens.
        /// </summary>
        public DbSet<AccessToken> AccessTokens { get; set; }

        /// <summary>
        /// Table set of all email change requests.
        /// </summary>
        public DbSet<EmailChangeRequest> EmailChangeRequests { get; set; }

        /// <summary>
        /// Table set of all restaurants.
        /// </summary>
        public DbSet<Restaurant> Restaurants { get; set; }

        /// <summary>
        /// Table set of all meals.
        /// </summary>
        public DbSet<Meal> Meals { get; set; }

        /// <summary>
        /// Table set of all takeouts.
        /// </summary>
        public DbSet<Takeout> Takeouts { get; set; }

        /// <summary>
        /// Table set of all takeout items.
        /// </summary>
        public DbSet<TakeoutItem> TakeoutItems { get; set; }

        /// <summary>
        /// Table set of all orders.
        /// </summary>
        public DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Construct SqlDatabase service.
        /// </summary>
        public SqlDatabase() : base(new DbContextOptionsBuilder().UseSqlServer(ConnectionString).Options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Base method call
            base.OnModelCreating(modelBuilder);

            // Map account class
            modelBuilder.Entity<Account>().HasKey(e => e.ID);
            modelBuilder.Entity<Account>().HasIndex(e => e.Username).IsUnique();
            modelBuilder.Entity<Account>().HasIndex(e => e.Email).IsUnique();

            // Map access token class
            modelBuilder.Entity<AccessToken>().HasKey(e => e.AccessID);
            modelBuilder.Entity<AccessToken>().HasIndex(e => new { e.AccessID, e.AccessKey });
            modelBuilder.Entity<AccessToken>().HasIndex(e => e.AccountID);

            // Map email change request class
            modelBuilder.Entity<EmailChangeRequest>().HasKey(e => e.ID);
            modelBuilder.Entity<EmailChangeRequest>().HasIndex(e => e.TargetAccount);

            // Map restaurant class
            modelBuilder.Entity<Restaurant>().HasKey(e => e.ID);

            // Map meal class
            modelBuilder.Entity<Meal>().HasKey(e => e.ID);

            // Map takeout item class
            modelBuilder.Entity<TakeoutItem>().HasKey(e => e.ID);
           
            // Map takeout class
            modelBuilder.Entity<Takeout>().HasKey(e => e.ID);
            modelBuilder.Entity<Takeout>().HasIndex(e => new { e.PlacedTime, e.ID });

            // Map order class
            modelBuilder.Entity<Order>().HasKey(e => e.ID);
            modelBuilder.Entity<Order>().HasIndex(e => new { e.PlacedTime, e.ID });
        }
    }
}
