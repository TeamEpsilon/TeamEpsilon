﻿using DeliveryWebApp.Database.Entries;
using DeliveryWebApp.Models.Account;
using DeliveryWebApp.Models.Admin;
using DeliveryWebApp.Models.Home;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DeliveryWebApp.Database
{
    public struct OpenState
    {
        /// <summary>
        /// Was action a success?
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Is the restaurant open now?
        /// </summary>
        public bool IsOpen { get; set; }
    }

    public interface ISqlRepository
    {
        /// <summary>
        /// Registers account into database.
        /// </summary>
        /// <param name="account">Account to register.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> RegisterAccountAsync(Account account);

        /// <summary>
        /// Check username availability.
        /// </summary>
        /// <param name="username">Desired username.</param>
        /// <returns>True if available, false otherwise.</returns>
        Task<bool> CheckUsernameAsync(string username);

        /// <summary>
        /// Check email availability.
        /// </summary>
        /// <param name="email">Desired email.</param>
        /// <returns>True if available, false otherwise.</returns>
        Task<bool> CheckEmailAsync(string email);

        /// <summary>
        /// Attempts to resend a verification email.
        /// </summary>
        /// <param name="email">Target email.</param>
        /// <returns>True on success, false of failure.</returns>
        Task<bool> ResendVerificationEmailAsync(string email);

        /// <summary>
        /// Attempts to verify account.
        /// </summary>
        /// <param name="email">Target email of the account.</param>
        /// <param name="code">Verification code.</param>
        /// <returns>True on success, false on failure.</returns>
        Task<bool> VerifyEmailAsync(string email, string code);

        /// <summary>
        /// Checks whether account is already verified.
        /// </summary>
        /// <param name="email">Account's email.</param>
        /// <returns>True if it is already verified, false otherwise.</returns>
        Task<bool> IsAccountVerifiedAsync(string email);

        /// <summary>
        /// Gets account by matching username or email.
        /// </summary>
        /// <param name="usernameOrEmail">Account's username or email.</param>
        /// <returns>Account if found, null otherwise.</returns>
        Task<Account> GetAccountAsync(string usernameOrEmail);

        /// <summary>
        /// Generates access token for an account.
        /// </summary>
        /// <param name="account">Target account.</param>
        /// <returns>Target account's access token.</returns>
        Task<AccessToken> GenerateAccessToken(Account account);

        /// <summary>
        /// Fetches account by access token.
        /// </summary>
        /// <param name="accessToken">Account's access token.</param>
        /// <returns>Account if access token is still valid, null otherwise.</returns>
        Task<Account> FetchAccountByAccessToken(AccessToken accessToken);

        /// <summary>
        /// Deletes account from the database.
        /// </summary>
        /// <param name="account">Target account.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> DeleteAccount(Account account);

        /// <summary>
        /// Updates isRestaurantOwner field.
        /// </summary>
        /// <param name="accountId">Target account's ID.</param>
        /// <param name="isOwner">Is owner or not.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> UpdateRestaurantOwner(Guid accountId, bool isOwner);

        /// <summary>
        /// Updates personal info.
        /// </summary>
        /// <param name="accountId">Target account's ID.</param>
        /// <param name="firstName">New first name.</param>
        /// <param name="lastName">New last name.</param>
        /// <param name="cellphoneNumber">New cellphone number.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> UpdatePersonalInfo(Guid accountId, string firstName, string lastName, string cellphoneNumber);

        /// <summary>
        /// Changes account password.
        /// </summary>
        /// <param name="accountId">Target account's ID.</param>
        /// <param name="oldPassword">Account's previous password.</param>
        /// <param name="newPassword">Account's new password.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> ChangePassword(Guid accountId, string oldPassword, string newPassword);

        /// <summary>
        /// Requests to change email if possible.
        /// </summary>
        /// <param name="accountId">Target account's ID.</param>
        /// <param name="newEmail">New email.</param>
        /// <returns>Valid GUID on success, zero GUID on failure.</returns>
        Task<Guid> RequestChangeEmail(Guid accountId, string newEmail);

        /// <summary>
        /// Attempts to verify change email request.
        /// </summary>
        /// <param name="id">Target request ID.</param>
        /// <param name="code">Verification code.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> AttemptVerifyEmailChangeRequest(Guid id, string code);

        /// <summary>
        /// Changes account avatar.
        /// </summary>
        /// <param name="accountId">Target account's ID.</param>
        /// <param name="avatarData">Avatar raw byte data.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> ChangeAvatar(Guid accountId, byte[] avatarData);

        /// <summary>
        /// Change account username.
        /// </summary>
        /// <param name="accountId">Target account's ID.</param>
        /// <param name="newUsername">New account's username.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> ChangeUsername(Guid accountId, string newUsername);

        /// <summary>
        /// Attempts to add or edit a restaurant.
        /// </summary>
        /// <param name="accountId">Owner of the restaurant.</param>
        /// <param name="model">Model of the restaurant.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> AttemptAddEditRestaurant(Guid accountId, AddRestaurantModel model);

        /// <summary>
        /// Fetches all of account's restaurants.
        /// </summary>
        /// <param name="accountId">Target accont id.</param>
        /// <returns>All of the restaurants owned by that account.</returns>
        Task<IEnumerable<MyRestaurantModel>> FetchAccountRestaurants(Guid accountId);

        /// <summary>
        /// Gets the restaurant if it is indeed owned by such account.
        /// </summary>
        /// <param name="accountId">Target account.</param>
        /// <param name="restaurantId">Target restaurant.</param>
        /// <param name="mealsAlso">Also fetch meals.</param>
        /// <returns>Restaurant if it is owned by account or null otherwise.</returns>
        Task<Restaurant> FetchRestaurantIfOwned(Guid accountId, Guid restaurantId, bool mealsAlso = false);

        /// <summary>
        /// Deletes the restaurant if it is owned by such account (and if password is correct as well).
        /// </summary>
        /// <param name="accountId">Target account ID.</param>
        /// <param name="restaurantId">Target restaurant ID.</param>
        /// <param name="password">Account password.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> DeleteRestaurantIfOwned(Guid accountId, Guid restaurantId, string password);

        /// <summary>
        /// Toggles open/closed state of the restaurant.
        /// </summary>
        /// <param name="accountId">Owner account ID.</param>
        /// <param name="restaurantId">Target restaurant ID.</param>
        /// <returns>Open state.</returns>
        Task<OpenState> ToggleOpenIfOwned(Guid accountId, Guid restaurantId);

        /// <summary>
        /// Fetches restaurants by desired state.
        /// </summary>
        /// <param name="state">Desired state.</param>
        /// <param name="offset">Desired offset.</param>
        /// <param name="count">Desired count.</param>
        /// <param name="search">Desired search string (filter).</param>
        /// <returns>List of restaurants.</returns>
        Task<IEnumerable<MyRestaurantModel>> FetchRestaurantsByState(RestaurantState state, int offset = 0, int count = 0, string search = "");

        /// <summary>
        /// Fetches restaurant by ID.
        /// </summary>
        /// <param name="id">Get restaurant by ID.</param>
        /// <param name="mealsAlso">Fetch meals also.</param>
        /// <returns>Restaurant on success, null on failure.</returns>
        Task<Restaurant> FetchRestaurant(Guid id, bool mealsAlso = false);

        /// <summary>
        /// Deletes a restaurant.
        /// </summary>
        /// <param name="id">Restaurant ID.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> DeleteRestaurant(Guid id);

        /// <summary>
        /// Toggles between open and closed state of a restaurant.
        /// </summary>
        /// <param name="id">Restaurant ID.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> ToggleOpen(Guid id);

        /// <summary>
        /// Toggles between verified and unverified state of a restaurant.
        /// </summary>
        /// <param name="id">Restaurant ID.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> ToggleVerify(Guid id);

        /// <summary>
        /// Changes restaurant picture if it is owned.
        /// </summary>
        /// <param name="restaurantId">Target restaurant ID.</param>
        /// <param name="accountId">Target account ID.</param>
        /// <param name="avatarData">Target avatar data.</param>
        /// <returns></returns>
        Task<bool> ChangeRestaurantPictureIfOwned(Guid restaurantId, Guid accountId, byte[] avatarData);

        /// <summary>
        /// Change role of a user.
        /// </summary>
        /// <param name="username">Target user's username.</param>
        /// <param name="role">Desired role.</param>
        /// <returns>True on success, false on failure.</returns>
        Task<bool> ChangeUserRole(string username, AccountType role);

        /// <summary>
        /// Finds all users that match these requirements.
        /// </summary>
        /// <param name="roles">Desired roles.</param>
        /// <param name="offset">Offset.</param>
        /// <param name="count">Amount.</param>
        /// <param name="search">Username or email substring.</param>
        /// <returns>List of found users.</returns>
        Task<IEnumerable<UserModel>> FindUsers(UserRole roles, int offset = 0, int count = 0, string search = "");

        /// <summary>
        /// Finds all restaurants which are open/closed at the moment.
        /// </summary>
        /// <param name="open">Do they need to be opened or closed?</param>
        /// <returns>List of restaurants.</returns>
        Task<IEnumerable<RestaurantModel>> FindRestaurants(bool open = true);

        /// <summary>
        /// Verifies users email address (for admin testing use only).
        /// </summary>
        /// <param name="username">Target account's username.</param>
        /// <returns>True if it was verified successfully, false otherwise.</returns>
        Task<bool> VerifyEmailAdminOnly(string username);

        /// <summary>
        /// Attempt to add or edit meal.
        /// </summary>
        /// <param name="amm">Meal model.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> AttemptAddEditMeal(AddMealModel amm);

        /// <summary>
        /// Changes meal picture if it is owned by account.
        /// </summary>
        /// <param name="restaurantId">Target restaurant ID.</param>
        /// <param name="mealId">Target meal ID.</param>
        /// <param name="accountId">Target account ID.</param>
        /// <param name="picData">Picture data.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> ChangeMealPictureIfOwned(Guid restaurantId, Guid mealId, Guid accountId, byte[] picData);

        /// <summary>
        /// Deletes a meal if it is owned.
        /// </summary>
        /// <param name="restaurantId">Restaurant ID.</param>
        /// <param name="mealId">Meal ID.</param>
        /// <param name="accountId">Account ID.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> DeleteMealIfOwned(Guid restaurantId, Guid mealId, Guid accountId);

        /// <summary>
        /// Adds a meal to cart if possible.
        /// </summary>
        /// <param name="restId">Restaurant ID.</param>
        /// <param name="mealId">Meal ID.</param>
        /// <param name="account">Account.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> AddMealToCart(Guid restId, Guid mealId, Account account);

        /// <summary>
        /// Gets all cart content.
        /// </summary>
        /// <param name="accountId">Target account ID.</param>
        /// <returns>Cart content list.</returns>
        Task<IEnumerable<TakeoutItem>> GetCartContent(Guid accountId);

        /// <summary>
        /// Increase or decrease amout of takeout item (qty).
        /// </summary>
        /// <param name="tkItemId">Takeout item ID.</param>
        /// <param name="accountId">Account ID.</param>
        /// <param name="incOrDec">True for increase, false for decrease.</param>
        /// <param name="fullyDelete">Fully delete all amounts (remove takeout item).</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> IncDecMeal(Guid tkItemId, Guid accountId, bool incOrDec = true, bool fullyDelete = false);

        /// <summary>
        /// Attempts to checkout the user.
        /// </summary>
        /// <param name="accountId">Target account ID.</param>
        /// <param name="model">User's checkout model.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> TryCheckout(Guid accountId, CheckoutModel model);

        /// <summary>
        /// Gets takeouts for restaurant (with TakeoutItems, PlacedBy and Meals also).
        /// </summary>
        /// <param name="restaurantId">Target restaurant ID.</param>
        /// <param name="pendingOnly">Fetch only pending takeouts.</param>
        /// <returns>List of takeouts.</returns>
        Task<IEnumerable<Takeout>> GetTakeouts(Guid restaurantId, bool pendingOnly = false);

        /// <summary>
        /// Gets takeout (and other stuff) if account is allowed to see it. Must NOT be already delivered.
        /// </summary>
        /// <param name="tkid">Takeout ID.</param>
        /// <param name="accid">Account ID (must be person who owns the takeout item restaurant, person who ordered, a delivery person, a dispatcher or an admin).</param>
        /// <returns>Takeout on success, null otherwise.</returns>
        Task<Takeout> SeeTakeoutIfAllowed(Guid tkid, Guid accid);

        /// <summary>
        /// Gets user's takeouts (all of them).
        /// </summary>
        /// <param name="accountId">Target account ID.</param>
        /// <returns>List of user takeouts.</returns>
        Task<IEnumerable<Takeout>> GetMyOrders(Guid accountId);

        /// <summary>
        /// Performs clean log out (clears all account-associated access tokens). This is good for delivery users.
        /// </summary>
        /// <param name="accountId">Target account ID.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> PerformsCleanLogOut(Guid accountId);

        /// <summary>
        /// Fetches a list of all available delivery users at this moment.
        /// </summary>
        /// <returns>List of avaliable delivery men.</returns>
        Task<IEnumerable<Account>> GetAvailableDeliveryBoys();

        /// <summary>
        /// Fetches all incomplete orders (commands) of a delivery man.
        /// </summary>
        /// <param name="username">Delivery man account username.</param>
        /// <returns>List of incomplete orders.</returns>
        Task<IEnumerable<Order>> FetchDelivererOrders(string username);

        /// <summary>
        /// Fetches latest completed order of a delivery man.
        /// </summary>
        /// <param name="username">Deliver man username.</param>
        /// <returns>Last completed order or null if there is none.</returns>
        Task<Order> GetLatestCompletedOrder(string username);

        /// <summary>
        /// Fetches takeouts that are deliverable by a delivery man (necessary orders).
        /// </summary>
        /// <param name="username">Target username of delivery boy.</param>
        /// <returns>List of takeouts.</returns>
        Task<IEnumerable<Takeout>> FetchDeliverableTakeouts(string username);

        /// <summary>
        /// Fetches all takeouts that can be picked up by a delivery man (possible orders).
        /// </summary>
        /// <returns>List of takeouts.</returns>
        Task<IEnumerable<Takeout>> FetchTakeoutsForPickup();

        /// <summary>
        /// Finds drop off order (and delivery man) for a takeout.
        /// </summary>
        /// <param name="takeout">Target takeout.</param>
        /// <returns>Order if exists, null otherwise.</returns>
        Task<Order> FindTakeoutDropOffOrder(Takeout takeout);


        /// <summary>
        /// Adds a pickup order.
        /// </summary>
        /// <param name="username">Target delivery man.</param>
        /// <param name="tkid">Target takeout.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> AddPickupOrder(string username, Guid tkid);

        /// <summary>
        /// Adds a dropoff order.
        /// </summary>
        /// <param name="username">Target delivery man.</param>
        /// <param name="tkid">Target takeout.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> AddDropoffOrder(string username, Guid tkid);

        /// <summary>
        /// Marks first order from delivery boy as complete.
        /// </summary>
        /// <param name="deliveryid">Target delivery man account ID.</param>
        /// <returns>True on success, false otherwise.</returns>
        Task<bool> MarkOrderComplete(Guid deliveryid);
    }
}
