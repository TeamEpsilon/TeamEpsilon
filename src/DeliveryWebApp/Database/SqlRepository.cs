﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DeliveryWebApp.Database.Entries;
using DeliveryWebApp.Models.Account;
using DeliveryWebApp.Models.Admin;
using DeliveryWebApp.Models.Home;
using DeliveryWebApp.Services;
using Microsoft.EntityFrameworkCore;

namespace DeliveryWebApp.Database
{
    public class SqlRepository : ISqlRepository
    {
        private IMailerBot _mailerBot;
        private IStorage _storage;
        private SqlDatabase _sqlDatabase;

        /// <summary>
        /// Create SQL repository.
        /// </summary>
        /// <param name="mailerBot">MailerBot dependency.</param>
        /// <param name="storage">Storage dependency.</param>
        /// <param name="sqlDatabase">SQL Database dependency.</param>
        public SqlRepository(IMailerBot mailerBot, IStorage storage, SqlDatabase sqlDatabase)
        {
            _mailerBot = mailerBot;
            _storage = storage;
            _sqlDatabase = sqlDatabase;
        }

        public async Task<bool> RegisterAccountAsync(Account account)
        {
            try
            {
                // If this account is the first account on this site, it becomes a special user and a administrator
                if (_sqlDatabase.Accounts.Count() == 0)
                {
                    account.AccountRole = AccountType.Administrator;
                    account.IsSpecial = true;
                }
                await _sqlDatabase.Accounts.AddAsync(account);
                await _sqlDatabase.SaveChangesAsync();
                await _mailerBot.SendVerificationCode(account.Email, account.VerificationCode);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public async Task<bool> CheckUsernameAsync(string username)
        {
            return _sqlDatabase.Accounts.Where(e => e.Username.ToLower() == username.ToLower()).Count() == 0;
        }

        public async Task<bool> CheckEmailAsync(string email)
        {
            return _sqlDatabase.Accounts.Where(e => e.Email.ToLower() == email.ToLower()).Count() == 0;
        }

        public async Task<bool> ResendVerificationEmailAsync(string email)
        {
            try
            {
                Account account = _sqlDatabase.Accounts.Where(e => (e.Email.ToLower() == email.ToLower()) && (e.IsVerified == false) && (e.RemainingVerificationResends > 0)).First();
                string code = Account.GenerateVerificationCode();
                await _mailerBot.SendVerificationCode(account.Email, code);
                account.VerificationCode = code;
                account.RemainingVerificationResends--;
                await _sqlDatabase.SaveChangesAsync();
            }
            catch
            {
                return false;
            }
            return true;
        }

        public async Task<bool> VerifyEmailAsync(string email, string code)
        {
            try
            {
                Account account = _sqlDatabase.Accounts.Where(e => (e.Email.ToLower() == email.ToLower()) && (e.IsVerified == false) && (e.RemainingVerificationTries > 0)).First();
                account.RemainingVerificationTries--;
                if (account.VerificationCode == code)
                {
                    account.IsVerified = true;
                    await _sqlDatabase.SaveChangesAsync();
                }
                else
                {
                    await _sqlDatabase.SaveChangesAsync();
                    return false;
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public async Task<bool> IsAccountVerifiedAsync(string email)
        {
            try
            {
                return (_sqlDatabase.Accounts.Where(e => (e.Email.ToLower() == email.ToLower()) && (e.IsVerified)).Count() > 0);
            }
            catch
            {
                return false;
            }
        }

        public async Task<Account> GetAccountAsync(string usernameOrEmail)
        {
            try
            {
                return _sqlDatabase.Accounts.Where(e => (e.Username.ToLower() == usernameOrEmail.ToLower()) || (e.Email.ToLower() == usernameOrEmail.ToLower())).First();
            }
            catch
            {
                return null;
            }
        }

        public async Task<AccessToken> GenerateAccessToken(Account account)
        {
            try
            {
                AccessToken accessToken = new AccessToken(account.ID);
                await _sqlDatabase.AccessTokens.AddAsync(accessToken);
                await _sqlDatabase.SaveChangesAsync();
                return accessToken;
            }
            catch
            {
                return null;
            }
        }

        public async Task<Account> FetchAccountByAccessToken(AccessToken accessToken)
        {
            try
            {
                AccessToken fetchedToken = _sqlDatabase.AccessTokens.Where(e => (e.AccessID == accessToken.AccessID) && (e.AccessKey == accessToken.AccessKey) && (e.Expiration > DateTime.UtcNow)).First();
                Account account = _sqlDatabase.Accounts.Where(e => (e.IsDeleted == false) && (e.ID == fetchedToken.AccountID)).First();
                if (account == null)
                {
                    return null;
                }
                fetchedToken.RefreshToken();
                await _sqlDatabase.SaveChangesAsync();
                return account;
            }
            catch
            {
                return null;
            }
        }

        public async Task<bool> DeleteAccount(Account account)
        {
            try
            {
                // Account must NOT own any restaurants and must NOT be a special account
                if ((!account.IsDeleted) && (_sqlDatabase.Restaurants.Where(e => e.Owner.ID == account.ID).Count() == 0) && (!account.IsSpecial))
                {
                    if (account.AccountRole == AccountType.DelivererUser)
                    {
                        if (((await FetchDelivererOrders(account.Username)).Count() > 0) || ((await FetchDeliverableTakeouts(account.Username)).Count() > 0))
                        {
                            return false;
                        }
                    }
                    foreach (Takeout myTakeout in await GetMyOrders(account.ID))
                    {
                        if (!myTakeout.IsDelivered)
                        {
                            return false;
                        }
                    }
                    account.IsDeleted = true;
                    account.Username = null;
                    account.Email = null;
                    await _sqlDatabase.SaveChangesAsync();
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> UpdateRestaurantOwner(Guid accountId, bool isOwner)
        {
            try
            {
                Account account = _sqlDatabase.Accounts.Where(e => e.ID == accountId).First();
                account.IsRestaurantOwner = isOwner;
                await _sqlDatabase.SaveChangesAsync();
                return true;
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> UpdatePersonalInfo(Guid accountId, string firstName, string lastName, string cellphoneNumber)
        {
            try
            {
                Account account = _sqlDatabase.Accounts.Where(e => e.ID == accountId).First();
                account.FirstName = firstName;
                account.LastName = lastName;
                account.CellphoneNumber = cellphoneNumber;
                await _sqlDatabase.SaveChangesAsync();
                return true;
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> ChangePassword(Guid accountId, string oldPassword, string newPassword)
        {
            try
            {
                Account account = _sqlDatabase.Accounts.Where(e => e.ID == accountId).First();
                if (account.CheckPassword(oldPassword))
                {
                    account.SetPassword(newPassword);
                    await _sqlDatabase.SaveChangesAsync();
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        public async Task<Guid> RequestChangeEmail(Guid accountId, string newEmail)
        {
            Guid guid = Guid.Empty;
            try
            {
                Account account = _sqlDatabase.Accounts.Where(e => (e.ID == accountId) && (e.RemainingVerificationResends > 0)).First();
                account.RemainingVerificationResends--;
                EmailChangeRequest ecr = new EmailChangeRequest(account.ID, newEmail);
                await _sqlDatabase.EmailChangeRequests.AddAsync(ecr);
                await _sqlDatabase.SaveChangesAsync();
                guid = ecr.ID;
                await _mailerBot.SendEmailChangeCode(newEmail, account.Username, ecr.VerificationCode, guid);
            }
            catch
            {
            }
            return guid;
        }

        public async Task<bool> AttemptVerifyEmailChangeRequest(Guid id, string code)
        {
            try
            {
                EmailChangeRequest ecr = _sqlDatabase.EmailChangeRequests.Where(e => (e.ID == id) && (e.VerificationCode == code) && (e.Expires > DateTime.UtcNow)).First();
                Account account = _sqlDatabase.Accounts.Where(e => (e.ID == ecr.TargetAccount) && (e.RemainingVerificationTries > 0)).First();
                account.RemainingVerificationTries--;
                account.Email = ecr.NewEmail;
                _sqlDatabase.EmailChangeRequests.Remove(ecr);
                await _sqlDatabase.SaveChangesAsync();
                return true;
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> ChangeAvatar(Guid accountId, byte[] avatarData)
        {
            try
            {
                Account account = _sqlDatabase.Accounts.Where(e => (e.ID == accountId)).First();
                string url = await _storage.Upload("avatars", account.ID.ToString() + "_" + Guid.NewGuid().ToString() + ".jpg", InfoHelper.ResizeCropImage(avatarData, 128, 128));
                if (url != null)
                {
                    account.ProfilePictureURL = url;
                    await _sqlDatabase.SaveChangesAsync();
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> ChangeUsername(Guid accountId, string newUsername)
        {
            try
            {
                Account account = _sqlDatabase.Accounts.Where(e => e.ID == accountId).First();
                account.Username = newUsername;
                await _sqlDatabase.SaveChangesAsync();
                return true;
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> AttemptAddEditRestaurant(Guid accountId, AddRestaurantModel model)
        {
            try
            {
                Restaurant restaurant;
                if (model.UpdateExisting)
                {
                    restaurant = _sqlDatabase.Restaurants.Where(e => e.ID == model.ExistingID).First();
                    if (restaurant.Owner.ID != accountId)
                    {
                        return false;
                    }
                }
                else
                {
                    Account account = _sqlDatabase.Accounts.Where(e => e.ID == accountId).First();
                    restaurant = new Restaurant();
                    restaurant.Owner = account;
                    _sqlDatabase.Restaurants.Add(restaurant);
                }
                restaurant.Name = model.Name;
                restaurant.Description = model.Description;
                restaurant.Address = model.Address;
                restaurant.MapLatitude = model.MapLatitude;
                restaurant.MapLongitude = model.MapLongitude;
                restaurant.ContactNumber = model.ContactNumber;
                restaurant.FaxNumber = model.FaxNumber;
                restaurant.OIB = model.OIB;
                restaurant.IBAN = model.IBAN;
                restaurant.Giro = model.Giro;
                restaurant.IsVerified = false;
                restaurant.IsOpen = false;
                await _sqlDatabase.SaveChangesAsync();
                return true;
            }
            catch
            {
            }
            return false;
        }

        public async Task<IEnumerable<MyRestaurantModel>> FetchAccountRestaurants(Guid accountId)
        {
            try
            {
                List<MyRestaurantModel> result = new List<MyRestaurantModel>();
                foreach (Restaurant restaurant in _sqlDatabase.Restaurants.Where(e => (e.Owner.ID == accountId)))
                {
                    MyRestaurantModel entry = new MyRestaurantModel(restaurant.ID, restaurant.Name, restaurant.IsVerified, !restaurant.IsOpen);
                    result.Add(entry);
                }
                return result;
            }
            catch
            {
            }
            return new MyRestaurantModel[0];
        }

        public async Task<Restaurant> FetchRestaurantIfOwned(Guid accountId, Guid restaurantId, bool mealsAlso)
        {
            try
            {
                if (mealsAlso)
                {
                    return _sqlDatabase.Restaurants.Include(e => e.Meals).Where(e => (e.Owner.ID == accountId) && (e.ID == restaurantId)).First();
                }
                return _sqlDatabase.Restaurants.Where(e => (e.Owner.ID == accountId) && (e.ID == restaurantId)).First();
            }
            catch
            {
            }
            return null;
        }

        public async Task<bool> DeleteRestaurantIfOwned(Guid accountId, Guid restaurantId, string password)
        {
            try
            {
                Account account = _sqlDatabase.Accounts.Where(e => e.ID == accountId).First();
                if (account.CheckPassword(password))
                {
                    Restaurant restaurant = _sqlDatabase.Restaurants.Include(e => e.Meals).Where(e => (e.Owner.ID == account.ID) && (e.ID == restaurantId)).First();
                    bool noPendingOrders = _sqlDatabase.TakeoutItems.Include(e => e.TargetTakeout).Include(e => e.TargetMeal).Where(e => (e.TargetMeal.ServedAt.ID == restaurantId) && (e.TargetTakeout != null) && ((!e.TargetTakeout.IsDelivered))).Count() == 0;
                    if ((restaurant != null) && (noPendingOrders))
                    {
                        foreach (Meal m in restaurant.Meals)
                        {
                            m.ServedAt = null;
                        }
                        _sqlDatabase.Restaurants.Remove(restaurant);
                        await _sqlDatabase.SaveChangesAsync();
                        return true;
                    }
                }
            }
            catch
            {
            }
            return false;
        }

        public async Task<OpenState> ToggleOpenIfOwned(Guid accountId, Guid restaurantId)
        {
            OpenState result = new OpenState()
            {
                Success = false,
                IsOpen = false
            };
            try
            {
                Account account = _sqlDatabase.Accounts.Where(e => e.ID == accountId).First();
                Restaurant restaurant = _sqlDatabase.Restaurants.Where(e => (e.Owner.ID == account.ID) && (e.ID == restaurantId)).First();
                if (restaurant.IsVerified)
                {
                    restaurant.IsOpen = !restaurant.IsOpen;
                    await _sqlDatabase.SaveChangesAsync();
                    result.Success = true;
                    result.IsOpen = restaurant.IsOpen;
                    return result;
                }
            }
            catch
            {
            }
            return result;
        }

        public async Task<IEnumerable<MyRestaurantModel>> FetchRestaurantsByState(RestaurantState state, int offset, int count, string search)
        {
            if (search is null)
            {
                search = "";
            }
            try
            {
                if (offset < 0)
                {
                    offset = 0;
                }
                IQueryable<Restaurant> restaurants = _sqlDatabase.Restaurants.Where(e => (state.HasFlag(RestaurantState.Closed) && (e.IsVerified == true) && (e.IsOpen == false)) || (state.HasFlag(RestaurantState.Open) && (e.IsVerified == true) && (e.IsOpen == true)) || (state.HasFlag(RestaurantState.Unverified) && (e.IsVerified == false))).Where(e => e.Name.ToLower().Contains(search.ToLower()));
                if ((restaurants.Count() == 0) || (offset > restaurants.Count()))
                {
                    return new MyRestaurantModel[0];
                }
                restaurants = restaurants.Skip(offset);
                if (restaurants.Count() == 0)
                {
                    return new MyRestaurantModel[0];
                }
                if (count <= 0)
                {
                    return restaurants.Select(e => new MyRestaurantModel(e.ID, e.Name, e.IsVerified, !e.IsOpen));
                }
                return restaurants.Take(Math.Min(count, restaurants.Count())).Select(e => new MyRestaurantModel(e.ID, e.Name, e.IsVerified, !e.IsOpen));
            }
            catch
            {
            }
            return new MyRestaurantModel[0];
        }

        public async Task<Restaurant> FetchRestaurant(Guid id, bool mealsAlso)
        {
            try
            {
                if (mealsAlso)
                {
                    return _sqlDatabase.Restaurants.Include(e => e.Owner).Include(e => e.Meals).Where(e => e.ID == id).First();
                }
                return _sqlDatabase.Restaurants.Include(e => e.Owner).Where(e => e.ID == id).First();
            }
            catch
            {
            }
            return null;
        }

        public async Task<bool> DeleteRestaurant(Guid id)
        {
            try
            {
                Restaurant restaurant = _sqlDatabase.Restaurants.Include(e => e.Owner).Include(e => e.Meals).Where(e => e.ID == id).First();
                bool noPendingOrders = _sqlDatabase.TakeoutItems.Include(e => e.TargetTakeout).Include(e => e.TargetMeal).Where(e => (e.TargetMeal.ServedAt.ID == id) && (e.TargetTakeout != null) && ((!e.TargetTakeout.IsDelivered))).Count() == 0;
                if ((restaurant != null) && (noPendingOrders))
                {
                    foreach (Meal m in restaurant.Meals)
                    {
                        m.ServedAt = null;
                    }
                    await _mailerBot.SendDeletedRestaurant(restaurant.Owner.Email, restaurant.Name);
                    _sqlDatabase.Restaurants.Remove(restaurant);
                    await _sqlDatabase.SaveChangesAsync();
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> ToggleOpen(Guid id)
        {
            try
            {
                Restaurant restaurant = _sqlDatabase.Restaurants.Include(e => e.Owner).Where(e => e.ID == id).First();
                if (restaurant != null)
                {
                    restaurant.IsOpen = !restaurant.IsOpen;
                    await _sqlDatabase.SaveChangesAsync();
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> ToggleVerify(Guid id)
        {
            try
            {
                Restaurant restaurant = _sqlDatabase.Restaurants.Include(e => e.Owner).Where(e => e.ID == id).First();
                if (restaurant != null)
                {
                    restaurant.IsVerified = !restaurant.IsVerified;
                    restaurant.IsOpen = false;
                    await _sqlDatabase.SaveChangesAsync();
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> ChangeRestaurantPictureIfOwned(Guid restaurantId, Guid accountId, byte[] avatarData)
        {
            try
            {
                Restaurant restaurant = _sqlDatabase.Restaurants.Where(e => (e.Owner.ID == accountId) && (e.ID == restaurantId)).First();
                string url = await _storage.Upload("restpics", restaurant.ID.ToString() + "_" + Guid.NewGuid().ToString() + ".jpg", InfoHelper.ResizeCropImage(avatarData, 640, 380));
                if (url != null)
                {
                    restaurant.PictureURL = url;
                    await _sqlDatabase.SaveChangesAsync();
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> ChangeUserRole(string username, AccountType role)
        {
            try
            {
                Account account = _sqlDatabase.Accounts.Where(e => e.Username.ToLower() == username.ToLower()).First();
                if ((account != null) && (!account.IsSpecial))
                {
                    if (account.AccountRole == AccountType.DelivererUser)
                    {
                        if (((await FetchDelivererOrders(account.Username)).Count() > 0) || ((await FetchDeliverableTakeouts(account.Username)).Count() > 0))
                        {
                            return false;
                        }
                    }
                    account.AccountRole = role;
                    await _sqlDatabase.SaveChangesAsync();
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        public async Task<IEnumerable<UserModel>> FindUsers(UserRole roles, int offset = 0, int count = 0, string search = "")
        {
            if (search is null)
            {
                search = "";
            }
            try
            {
                if (offset < 0)
                {
                    offset = 0;
                }
                IQueryable<Account> users = _sqlDatabase.Accounts.Where(e => (roles.HasFlag(UserRole.RegularUser) && (e.AccountRole == AccountType.RegularUser)) || (roles.HasFlag(UserRole.DeliveryUser) && (e.AccountRole == AccountType.DelivererUser)) || (roles.HasFlag(UserRole.DispatcherUser) && (e.AccountRole == AccountType.DispatcherUser)) || (roles.HasFlag(UserRole.AdminUser) && (e.AccountRole == AccountType.Administrator))).Where(e => (e.Username.ToLower().Contains(search.ToLower())) || (e.Email.ToLower().Contains(search.ToLower()))).Where(e => !e.IsDeleted);
                if ((users.Count() == 0) || (offset > users.Count()))
                {
                    return new UserModel[0];
                }
                users = users.Skip(offset);
                if (users.Count() == 0)
                {
                    return new UserModel[0];
                }
                if (count <= 0)
                {
                    return users.Select(e => new UserModel() { Username = e.Username, Email = e.Email, IsVerified = e.IsVerified, Role = e.AccountRole });
                }
                return users.Take(Math.Min(count, users.Count())).Select(e => new UserModel() { Username = e.Username, Email = e.Email, IsVerified = e.IsVerified, Role = e.AccountRole });
            }
            catch
            {
            }
            return new UserModel[0];
        }

        public async Task<IEnumerable<RestaurantModel>> FindRestaurants(bool open)
        {
            try
            {
                return _sqlDatabase.Restaurants.Where(e => e.IsVerified && (e.IsOpen == open)).Select(e => new RestaurantModel() { ID = e.ID, Name = e.Name, Description = e.Description, PictureURL = e.PictureURL });
            }
            catch
            {
            }
            return new RestaurantModel[0];
        }

        public async Task<bool> VerifyEmailAdminOnly(string username)
        {
            try
            {
                Account targetAccount = await GetAccountAsync(username);
                if ((targetAccount != null) && (!targetAccount.IsVerified))
                {
                    targetAccount.IsVerified = true;
                    await _sqlDatabase.SaveChangesAsync();
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> AttemptAddEditMeal(AddMealModel amm)
        {
            try
            {
                Meal meal;
                if (amm.EditExisting)
                {
                    meal = _sqlDatabase.Meals.Where(e => (e.ID == amm.EditID) && (e.ServedAt.ID == amm.RestaurantID)).First();
                    if (_sqlDatabase.TakeoutItems.Include(e => e.TargetTakeout).Include(e => e.TargetMeal).Where(e => (e.TargetMeal != null) && (e.TargetMeal.ID == meal.ID) && (e.TargetTakeout != null) && (!e.TargetTakeout.IsDelivered)).Count() > 0)
                    {
                        return false;
                    }
                }
                else
                {
                    meal = new Meal()
                    {
                        ServedAt = _sqlDatabase.Restaurants.Where(e => e.ID == amm.RestaurantID).First()
                    };
                    _sqlDatabase.Meals.Add(meal);
                }
                meal.Name = amm.Name;
                meal.Description = amm.Description;
                meal.Price = (uint)(amm.Price * 100);
                meal.PreparationMinutes = amm.PrepTime;
                await _sqlDatabase.SaveChangesAsync();
                return true;
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> ChangeMealPictureIfOwned(Guid restaurantId, Guid mealId, Guid accountId, byte[] picData)
        {
            try
            {
                Restaurant restaurant = _sqlDatabase.Restaurants.Include(e => e.Meals).Where(e => (e.Owner.ID == accountId) && (e.ID == restaurantId)).First();
                Meal meal = restaurant.Meals.Where(e => e.ID == mealId).First();
                string url = await _storage.Upload("mealpics", meal.ID.ToString() + "_" + Guid.NewGuid().ToString() + ".jpg", InfoHelper.ResizeCropImage(picData, 128, 128));
                if (url != null)
                {
                    meal.PictureURL = url;
                    await _sqlDatabase.SaveChangesAsync();
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> DeleteMealIfOwned(Guid restaurantId, Guid mealId, Guid accountId)
        {
            try
            {
                Restaurant restaurant = _sqlDatabase.Restaurants.Include(e => e.Meals).Where(e => (e.Owner.ID == accountId) && (e.ID == restaurantId)).First();
                Meal meal = restaurant.Meals.Where(e => e.ID == mealId).First();
                if (meal != null)
                {
                    if (_sqlDatabase.TakeoutItems.Include(e => e.TargetTakeout).Where(e => (e.TargetMeal != null) && (e.TargetMeal.ID == meal.ID) && (e.TargetTakeout != null) && (!e.TargetTakeout.IsDelivered)).Count() > 0)
                    {
                        return false;
                    }
                    foreach (TakeoutItem tki in _sqlDatabase.TakeoutItems.Where(e => (e.TargetMeal != null) && (e.TargetMeal.ID == meal.ID)))
                    {
                        tki.TargetMeal = null;
                    }
                    _sqlDatabase.Meals.Remove(meal);
                    await _sqlDatabase.SaveChangesAsync();
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> AddMealToCart(Guid restId, Guid mealId, Account account)
        {
            try
            {
                Restaurant restaurant = _sqlDatabase.Restaurants.Include(e => e.Meals).Where(e => (e.ID == restId) && (e.IsOpen)).First();
                Meal meal = restaurant.Meals.Where(e => e.ID == mealId).First();
                if (meal != null)
                {
                    TakeoutItem takeoutItem = null;
                    try
                    {
                        takeoutItem = _sqlDatabase.TakeoutItems.Where(e => (e.CreatedBy.ID == account.ID) && (e.TargetMeal.ID == mealId) && (e.TargetTakeout == null)).First();
                    } catch { }
                    if (takeoutItem == null)
                    {
                        takeoutItem = new TakeoutItem()
                        {
                            CreatedBy = account,
                            Quantity = 1,
                            TargetMeal = meal,
                            TargetTakeout = null,
                            Comment = ""
                        };
                        _sqlDatabase.TakeoutItems.Add(takeoutItem);
                    }
                    else
                    {
                        takeoutItem.Quantity++;
                    }
                    await _sqlDatabase.SaveChangesAsync();
                    return true;
                }
                return true;
            }
            catch { }
            return false;
        }

        public async Task<IEnumerable<TakeoutItem>> GetCartContent(Guid accountId)
        {
            try
            {
                List<TakeoutItem> list = new List<TakeoutItem>();
                IEnumerable<TakeoutItem> tmpList = _sqlDatabase.TakeoutItems.Include(e => e.TargetMeal).Include(e => e.TargetMeal.ServedAt).Where(e => (e.CreatedBy.ID == accountId) && (e.TargetTakeout == null)).ToList();
                foreach (TakeoutItem itm in tmpList)
                {
                    if ((itm.TargetMeal == null) || (itm.TargetMeal.ServedAt == null))
                    {
                        _sqlDatabase.TakeoutItems.Remove(itm);
                    }
                    else
                    {
                        list.Add(itm);
                    }
                }
                await _sqlDatabase.SaveChangesAsync();
                return list;
            }
            catch { }
            return new TakeoutItem[0];
        }

        public async Task<bool> IncDecMeal(Guid tkItemId, Guid accountId, bool incOrDec, bool fullyDelete)
        {
            try
            {
                TakeoutItem takeout = _sqlDatabase.TakeoutItems.Where(e => (e.ID == tkItemId) && (e.CreatedBy.ID == accountId) && (e.TargetTakeout == null)).First();
                if (fullyDelete)
                {
                    _sqlDatabase.TakeoutItems.Remove(takeout);
                }
                else if (incOrDec)
                {
                    takeout.Quantity++;
                }
                else if (takeout.Quantity > 1)
                {
                    takeout.Quantity--;
                }
                else if (takeout.Quantity == 1)
                {
                    _sqlDatabase.TakeoutItems.Remove(takeout);
                }
                else
                {
                    return false;
                }
                await _sqlDatabase.SaveChangesAsync();
                return true;
            }
            catch { }
            return false;
        }

        public async Task<bool> TryCheckout(Guid accountId, CheckoutModel model)
        {
            try
            {
                Account account = _sqlDatabase.Accounts.Where(e => (e.ID == accountId) && (!e.IsDeleted)).First();
                List<TakeoutItem> list = new List<TakeoutItem>();
                IEnumerable<TakeoutItem> tmpList = _sqlDatabase.TakeoutItems.Include(e => e.TargetMeal).Include(e => e.TargetMeal.ServedAt).Where(e => (e.CreatedBy.ID == accountId) && (e.TargetTakeout == null)).ToList();
                foreach (TakeoutItem itm in tmpList)
                {
                    if ((itm.TargetMeal == null) || (itm.TargetMeal.ServedAt == null) || (!itm.TargetMeal.ServedAt.IsOpen))
                    {
                        return false;
                    }
                    else
                    {
                        list.Add(itm);
                    }
                }
                IEnumerable<Guid> restaurantGuids = list.Select(e => e.TargetMeal.ServedAt.ID).Distinct();
                foreach (Guid restaurantId in restaurantGuids)
                {
                    IEnumerable<TakeoutItem> restaurantItems = list.Where(e => e.TargetMeal.ServedAt.ID == restaurantId);
                    Restaurant restaurant = _sqlDatabase.Restaurants.Include(e => e.Owner).Where(e => e.ID == restaurantId).First();

                    Takeout takeout = new Takeout()
                    {
                        Address = model.Address,
                        IsDelivered = false,
                        MapLatitude = model.MapLatitude,
                        MapLongitude = model.MapLongitude,
                        PlacedBy = account,
                        PlacedTime = DateTime.UtcNow
                    };
                    await _sqlDatabase.Takeouts.AddAsync(takeout);

                    foreach (TakeoutItem item in restaurantItems)
                    {
                        item.TargetTakeout = takeout;
                    }

                    await _sqlDatabase.SaveChangesAsync();

                    await _mailerBot.SendOrderDetails(restaurant.Owner.Email, restaurant.Name, restaurantItems, takeout, account);
                }
                return true;
            } catch { }
            return false;
        }

        public async Task<IEnumerable<Takeout>> GetTakeouts(Guid restaurantId, bool pendingOnly = false)
        {
            try
            {
                List<Takeout> result = new List<Takeout>();
                List<Takeout> takeouts = _sqlDatabase.Takeouts.Include(e => e.PlacedBy).Include(e => e.TakeoutItems).ThenInclude(e => e.TargetMeal).ThenInclude(e => e.ServedAt).Where(e => (e.TakeoutItems.Count() > 0) && (!pendingOnly || !e.IsDelivered)).ToList();
                foreach (Takeout t in takeouts)
                {
                    if (t.TakeoutItems.First().TargetMeal.ServedAt.ID == restaurantId)
                    {
                        result.Add(t);
                    }
                }
                return result.OrderByDescending(e => e.PlacedTime);
            }
            catch { }
            return new Takeout[0];
        }

        public async Task<Takeout> SeeTakeoutIfAllowed(Guid tkid, Guid accid)
        {
            try
            {
                Takeout takeout = _sqlDatabase.Takeouts.Include(e => e.PlacedBy).Include(e => e.TakeoutItems).ThenInclude(e => e.TargetMeal).ThenInclude(e => e.ServedAt).ThenInclude(e => e.Owner).Where(e => (e.ID == tkid)).First();
                if (takeout.IsDelivered)
                {
                    return null;
                }
                Account account = _sqlDatabase.Accounts.Where(e => (e.ID == accid) && (!e.IsDeleted)).First();
                if ((takeout.TakeoutItems.First().TargetMeal.ServedAt.Owner.ID == account.ID) || (takeout.PlacedBy.ID == account.ID) || (account.AccountRole == AccountType.DelivererUser) || (account.AccountRole == AccountType.DispatcherUser) || (account.AccountRole == AccountType.Administrator))
                {
                    return takeout;
                }
            }
            catch { }
            return null;
        }

        public async Task<IEnumerable<Takeout>> GetMyOrders(Guid accountId)
        {
            try
            {
                return _sqlDatabase.Takeouts.Include(e => e.PlacedBy).Include(e => e.TakeoutItems).ThenInclude(e => e.TargetMeal).ThenInclude(e => e.ServedAt).Where(e => (e.PlacedBy.ID == accountId)).OrderByDescending(e => e.PlacedTime);
            }
            catch { }
            return new Takeout[0];
        }

        public async Task<bool> PerformsCleanLogOut(Guid accountId)
        {
            try
            {
                List<AccessToken> accessTokens = _sqlDatabase.AccessTokens.Where(e => e.AccountID == accountId).ToList();
                foreach (AccessToken atk in accessTokens)
                {
                    _sqlDatabase.AccessTokens.Remove(atk);
                }
                await _sqlDatabase.SaveChangesAsync();
                return true;
            }
            catch
            {
            }
            return false;
        }

        public async Task<IEnumerable<Account>> GetAvailableDeliveryBoys()
        {
            try
            {
                IQueryable<Guid> onlineGuids = _sqlDatabase.AccessTokens.Where(e => e.Expiration > DateTime.UtcNow).Select(e => e.AccountID);
                return _sqlDatabase.Accounts.Where(e => (!e.IsDeleted) && (e.AccountRole == AccountType.DelivererUser) && (onlineGuids.Contains(e.ID)));
            } catch { }
            return new Account[0];
        }

        public async Task<IEnumerable<Order>> FetchDelivererOrders(string username)
        {
            try
            {
                return _sqlDatabase.Orders.Include(e => e.TargetedItem).Include(e => e.Assignee).Where(e => (e.Assignee.Username.ToLower() == username.ToLower()) && (!e.IsCompleted)).ToList();
            }
            catch
            {
            }
            return new Order[0];
        }

        public async Task<Order> GetLatestCompletedOrder(string username)
        {
            try
            {
                return _sqlDatabase.Orders.Include(e => e.Assignee).Where(e => (e.Assignee.Username.ToLower() == username.ToLower()) && (e.IsCompleted) && (e.Type == OrderType.GoToLocation)).OrderByDescending(e => e.PlacedTime).First();
            } catch { }
            return null;
        }

        public async Task<IEnumerable<Takeout>> FetchDeliverableTakeouts(string username)
        {
            try
            {
                IQueryable<Guid> pickedUp = _sqlDatabase.Orders.Include(e => e.Assignee).Include(e => e.TargetedItem).Where(e => (e.TargetedItem != null) && (e.Type == OrderType.PickUpTakeoutItem) && (e.Assignee.Username.ToLower() == username.ToLower())).Select(e => e.TargetedItem.ID).Distinct();
                IQueryable<Guid> droppedOff = _sqlDatabase.Orders.Include(e => e.Assignee).Include(e => e.TargetedItem).Where(e => (e.TargetedItem != null) && (e.Type == OrderType.DropOffTakeoutItem) && (e.Assignee.Username.ToLower() == username.ToLower())).Select(e => e.TargetedItem.ID).Distinct();
                return _sqlDatabase.Takeouts.Include(e => e.PlacedBy).Where(e => (!e.IsDelivered) && (pickedUp.Contains(e.ID)) && (!droppedOff.Contains(e.ID))).OrderByDescending(e => e.PlacedTime).ToList();
            } catch { }
            return new Takeout[0];
        }

        public async Task<IEnumerable<Takeout>> FetchTakeoutsForPickup()
        {
            try
            {
                IQueryable<Guid> orders = _sqlDatabase.Orders.Include(e => e.TargetedItem).Where(e => (e.TargetedItem != null) && (e.Type != OrderType.GoToLocation)).Select(e => e.TargetedItem.ID).Distinct();
                return _sqlDatabase.Takeouts.Include(e => e.PlacedBy).Where(e => (!e.IsDelivered) && (!orders.Contains(e.ID))).OrderByDescending(e => e.PlacedTime).ToList();
            }
            catch { }
            return new Takeout[0];
        }

        public async Task<Order> FindTakeoutDropOffOrder(Takeout takeout)
        {
            try
            {
                return _sqlDatabase.Orders.Include(e => e.Assignee).Include(e => e.TargetedItem).Where(e => (e.TargetedItem.ID == takeout.ID) && (e.Type == OrderType.DropOffTakeoutItem)).First();
            } catch { }
            return null;
        }

        public async Task<bool> AddPickupOrder(string username, Guid tkid)
        {
            try
            {
                Account deliveryMan = await GetAccountAsync(username);
                if (deliveryMan.AccountRole == AccountType.DelivererUser)
                {
                    Takeout takeout = await SeeTakeoutIfAllowed(tkid, deliveryMan.ID);
                    Restaurant restaurant = takeout.TakeoutItems.First().TargetMeal.ServedAt;

                    Order order1 = new Order()
                    {
                        Assignee = deliveryMan,
                        Address = restaurant.Address,
                        MapLatitude = restaurant.MapLatitude,
                        MapLongitude = restaurant.MapLongitude,
                        PlacedTime = DateTime.UtcNow,
                        TargetedItem = null,
                        Type = OrderType.GoToLocation
                    };

                    Order order2 = new Order()
                    {
                        Assignee = deliveryMan,
                        Address = restaurant.Address,
                        MapLatitude = restaurant.MapLatitude,
                        MapLongitude = restaurant.MapLongitude,
                        PlacedTime = DateTime.UtcNow,
                        TargetedItem = takeout,
                        Type = OrderType.PickUpTakeoutItem
                    };

                    await _sqlDatabase.Orders.AddAsync(order1);
                    await _sqlDatabase.Orders.AddAsync(order2);
                    await _sqlDatabase.SaveChangesAsync();

                    return true;
                }
            } catch { }
            return false;
        }

        public async Task<bool> AddDropoffOrder(string username, Guid tkid)
        {
            try
            {
                Account deliveryMan = await GetAccountAsync(username);
                if (deliveryMan.AccountRole == AccountType.DelivererUser)
                {
                    Takeout takeout = await SeeTakeoutIfAllowed(tkid, deliveryMan.ID);

                    Order order1 = new Order()
                    {
                        Assignee = deliveryMan,
                        Address = takeout.Address,
                        MapLatitude = takeout.MapLatitude,
                        MapLongitude = takeout.MapLongitude,
                        PlacedTime = DateTime.UtcNow,
                        TargetedItem = null,
                        Type = OrderType.GoToLocation
                    };

                    Order order2 = new Order()
                    {
                        Assignee = deliveryMan,
                        Address = takeout.Address,
                        MapLatitude = takeout.MapLatitude,
                        MapLongitude = takeout.MapLongitude,
                        PlacedTime = DateTime.UtcNow,
                        TargetedItem = takeout,
                        Type = OrderType.DropOffTakeoutItem
                    };

                    await _sqlDatabase.Orders.AddAsync(order1);
                    await _sqlDatabase.Orders.AddAsync(order2);
                    await _sqlDatabase.SaveChangesAsync();

                    return true;
                }
            }
            catch { }
            return false;
        }

        public async Task<bool> MarkOrderComplete(Guid deliveryid)
        {
            try
            {
                Order order = _sqlDatabase.Orders.Include(e => e.TargetedItem).Include(e => e.Assignee).Where(e => (e.Assignee.ID == deliveryid) && (!e.IsCompleted)).First();
                order.IsCompleted = true;
                order.CompletedTime = DateTime.UtcNow;
                if (order.Type == OrderType.DropOffTakeoutItem)
                {
                    order.TargetedItem.IsDelivered = true;
                    order.TargetedItem.DeliveredTime = DateTime.UtcNow;
                }
                await _sqlDatabase.SaveChangesAsync();
                return true;
            } catch { }
            return false;
        }
    }
}
