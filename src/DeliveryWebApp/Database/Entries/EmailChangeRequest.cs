﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp.Database.Entries
{
    public class EmailChangeRequest
    {
        /// <summary>
        /// Request ID (unique).
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// Affected account.
        /// </summary>
        public Guid TargetAccount { get; set; }

        /// <summary>
        /// New email.
        /// </summary>
        public string NewEmail { get; set; }

        /// <summary>
        /// Verification code.
        /// </summary>
        public string VerificationCode { get; set; }

        /// <summary>
        /// Expiration date and time.
        /// </summary>
        public DateTime Expires { get; set; }

        private static Random _random = new Random();

        /// <summary>
        /// Required by Entity Framework.
        /// </summary>
        public EmailChangeRequest()
        {
            TargetAccount = Guid.Empty;
            NewEmail = null;
            GenerateVerificationCode();
            UpdateExpiration();
        }

        /// <summary>
        /// Creates a new email change request.
        /// </summary>
        /// <param name="targetAccount">Target account's ID.</param>
        /// <param name="newEmail">New email.</param>
        public EmailChangeRequest(Guid targetAccount, string newEmail) : this()
        {
            TargetAccount = targetAccount;
            NewEmail = newEmail;
        }

        private void GenerateVerificationCode()
        {
            VerificationCode = _random.Next(10000, 99999).ToString();
        }

        public void UpdateExpiration()
        {
            Expires = DateTime.UtcNow.AddSeconds(Info.ChangeEmailRequestDuration);
        }
    }
}
