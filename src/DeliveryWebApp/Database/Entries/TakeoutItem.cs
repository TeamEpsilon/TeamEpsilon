﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp.Database.Entries
{
    public class TakeoutItem
    {
        /// <summary>
        /// Takeout item ID.
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// Targeted meal.
        /// </summary>
        public virtual Meal TargetMeal { get; set; }

        /// <summary>
        /// Targeted takeout.
        /// </summary>
        public Takeout TargetTakeout { get; set; }

        /// <summary>
        /// Amount of items to order.
        /// </summary>
        public uint Quantity { get; set; }

        /// <summary>
        /// Additional comment.
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Account that created this takeout item.
        /// </summary>
        public Account CreatedBy { get; set; }
    }
}
