﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp.Database.Entries
{
    public class AccessToken
    {
        /// <summary>
        /// Access token's ID (unique).
        /// </summary>
        public Guid AccessID { get; set; }

        /// <summary>
        /// Access token's access key.
        /// </summary>
        public Guid AccessKey { get; set; }

        /// <summary>
        /// Access token's account ID.
        /// </summary>
        public Guid AccountID { get; set; }

        /// <summary>
        /// Access token's expiration time.
        /// </summary>
        public DateTime Expiration { get; set; }

        /// <summary>
        /// Required by Entity Framework.
        /// </summary>
        public AccessToken()
        {
            AccessKey = Guid.NewGuid();
            AccountID = Guid.Empty;
            RefreshToken();
        }

        /// <summary>
        /// Creates new access token for account ID.
        /// </summary>
        /// <param name="accountId">Target account's ID.</param>
        public AccessToken(Guid accountId) : this()
        {
            AccountID = accountId;
        }

        /// <summary>
        /// Creates simulated access token from access ID and access key.
        /// </summary>
        /// <param name="accessId">Original access ID.</param>
        /// <param name="accessKey">Original access key.</param>
        public AccessToken(Guid accessId, Guid accessKey)
        {
            AccessID = accessId;
            AccessKey = accessKey;
        }

        /// <summary>
        /// Updates (delays) token expiration.
        /// </summary>
        public void RefreshToken()
        {
            Expiration = DateTime.UtcNow.AddSeconds(Info.AccessTokenValidityDuration);
        }
    }
}
