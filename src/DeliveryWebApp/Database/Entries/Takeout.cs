﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp.Database.Entries
{
    public class Takeout
    {
        /// <summary>
        /// Takeout ID.
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// Account that has placed this takeout request.
        /// </summary>
        public Account PlacedBy { get; set; }

        /// <summary>
        /// All takeout items belonging to this class.
        /// </summary>
        public ICollection<TakeoutItem> TakeoutItems { get; set; }

        /// <summary>
        /// Map logitude of the delivery.
        /// </summary>
        public double MapLongitude { get; set; }

        /// <summary>
        /// Map latitude of the delivery.
        /// </summary>
        public double MapLatitude { get; set; }

        /// <summary>
        /// Address of the delivery.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Time the takeout was placed.
        /// </summary>
        public DateTime PlacedTime { get; set; }

        /// <summary>
        /// Is takout delivered.
        /// </summary>
        public bool IsDelivered { get; set; }

        /// <summary>
        /// Time the takeout was delivered.
        /// </summary>
        public DateTime DeliveredTime { get; set; }
    }
}
