﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp.Database.Entries
{
    public class Meal
    {
        /// <summary>
        /// Meal ID.
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// Restaurant where this meal is served at.
        /// </summary>
        public Restaurant ServedAt { get; set; }

        /// <summary>
        /// Meal name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Meal description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Meal price (times 100).
        /// </summary>
        public uint Price { get; set; }

        /// <summary>
        /// Meal picture URL.
        /// </summary>
        public string PictureURL { get; set; } = "/img/avatar/meal_128.png";

        /// <summary>
        /// Time (in minutes) to prepare meal (approx.)
        /// </summary>
        public uint PreparationMinutes { get; set; }

        /// <summary>
        /// Takeout items that are targeting this meal.
        /// </summary>
        public virtual ICollection<TakeoutItem> TakeoutItems { get; set; }
    }
}
