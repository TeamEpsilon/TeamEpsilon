﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp.Database.Entries
{
    public class Restaurant
    {
        /// <summary>
        /// Restaurant unique ID.
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// Restaurant owner's account.
        /// </summary>
        public Account Owner { get; set; }

        /// <summary>
        /// Restaurant name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Restaurant description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Restaurant address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Restaurant latitude.
        /// </summary>
        public double MapLatitude { get; set; }

        /// <summary>
        /// Restaurant longitude.
        /// </summary>
        public double MapLongitude { get; set; }

        /// <summary>
        /// Restaurant contact number.
        /// </summary>
        public string ContactNumber { get; set; }
        
        /// <summary>
        /// Restaurant fax number.
        /// </summary>
        public string FaxNumber { get; set; }

        /// <summary>
        /// Restaurant OIB.
        /// </summary>
        public string OIB { get; set; }
        
        /// <summary>
        /// Restaurant IBAN.
        /// </summary>
        public string IBAN { get; set; }

        /// <summary>
        /// Restaurant giro bank account.
        /// </summary>
        public string Giro { get; set; }

        /// <summary>
        /// Restaurant picture URL.
        /// </summary>
        public string PictureURL { get; set; } = "/img/avatar/RD_640_380.png";

        /// <summary>
        /// Served meals at this restaurant.
        /// </summary>
        public ICollection<Meal> Meals { get; set; }

        /// <summary>
        /// Is restaurant verified.
        /// </summary>
        public bool IsVerified { get; set; } = false;

        /// <summary>
        /// Is restaurant currently open.
        /// </summary>
        public bool IsOpen { get; set; } = false;
    }
}
