﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryWebApp.Database.Entries
{
    public enum AccountType
    {
        RegularUser,
        DelivererUser,
        DispatcherUser,
        Administrator
    }
	
    public class Account
    {
        /// <summary>
        /// Account's ID (unique).
        /// </summary>
        public Guid ID { get; set; }
        /// <summary>
        /// Account's username (unique).
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Account's email address (unique).
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Account's password hashed.
        /// </summary>
        public byte[] PasswordHashed { get; set; }
        /// <summary>
        /// Account's first name.
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Account's last name.
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Account's cellphone number.
        /// </summary>
        public string CellphoneNumber { get; set; }
        /// <summary>
        /// Account's profile picture URL.
        /// </summary>
        public string ProfilePictureURL { get; set; }
        /// <summary>
        /// Account's verification code (used for email verification).
        /// </summary>
        public string VerificationCode { get; set; }
        /// <summary>
        /// True if account is verified and usable, false if it requires to be verified.
        /// </summary>
        public bool IsVerified { get; set; }

        /// <summary>
        /// Remaining times to try verify account.
        /// </summary>
        public int RemainingVerificationTries { get; set; }

        /// <summary>
        /// Remaining times to resend verification codes.
        /// </summary>
        public int RemainingVerificationResends { get; set; }

        /// <summary>
        /// Determines whether the account is deleted or not.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Determines whether restaurant panel will be shown or not.
        /// </summary>
        public bool IsRestaurantOwner { get; set; }
		
        /// <summary>
        /// Determines user's role in the delivery process.
        /// </summary>
        public AccountType AccountRole { get; set; } = AccountType.RegularUser;

        /// <summary>
        /// Account's owned restaurants.
        /// </summary>
        public ICollection<Restaurant> Restaurants { get; set; }

        /// <summary>
        /// Account's orders (used only if account is a deliverer).
        /// </summary>
        public ICollection<Order> Orders { get; set; }

        /// <summary>
        /// List of placed takeouts by this account.
        /// </summary>
        public ICollection<Takeout> PlacedTakeouts { get; set; }

        /// <summary>
        /// List of takeout items created by this account.
        /// </summary>
        public ICollection<TakeoutItem> PlacedTakeoutItems { get; set; }

        /// <summary>
        /// Special users cannot delete their accounts or change their roles.
        /// </summary>
        public bool IsSpecial { get; set; } = false;

        private const string defaultProfilePictureUrl = "/img/avatar/avatar_128.png";
        private static SHA512 sha512 = SHA512.Create();
        private static Random random = new Random();
        
        /// <summary>
        /// Required by Entity Framework.
        /// </summary>
        public Account()
        {
            Username = null;
            Email = null;
            PasswordHashed = null;
            FirstName = null;
            LastName = null;
            CellphoneNumber = null;
            ProfilePictureURL = defaultProfilePictureUrl;
            GenerateNewVerificationCode();
            IsVerified = false;
            RemainingVerificationTries = 24;
            RemainingVerificationResends = 8;
            IsDeleted = false;
            IsRestaurantOwner = false;
            AccountRole = AccountType.RegularUser;
        }

        /// <summary>
        /// Creates an account entry container.
        /// </summary>
        /// <param name="username">Account's username.</param>
        /// <param name="email">Account's email address.</param>
        /// <param name="password">Account's password.</param>
        /// <param name="firstName">Account's first name.</param>
        /// <param name="lastName">Account's last name.</param>
        /// <param name="cellphoneNumber">Account's cellphone number.</param>
        public Account(string username, string email, string password, string firstName, string lastName, string cellphoneNumber) : this()
        {
            Username = username;
            Email = email;
            SetPassword(password);
            FirstName = firstName;
            LastName = lastName;
            CellphoneNumber = cellphoneNumber;
        }

        /// <summary>
        /// Generate verification code.
        /// </summary>
        /// <returns>Verification code.</returns>
        public static string GenerateVerificationCode()
        {
            // Generate random 5-digit string combination
            return random.Next(10000, 99999).ToString();
        }

        /// <summary>
        /// Generates new verification code.
        /// </summary>
        public void GenerateNewVerificationCode()
        {
            VerificationCode = GenerateVerificationCode();
        }

        /// <summary>
        /// Checks if the account's password is correct.
        /// </summary>
        /// <param name="password">Password to check.</param>
        /// <returns>True if password is correct, false otherwise.</returns>
        public bool CheckPassword(string password)
        {
            return PasswordHashed.SequenceEqual(HashPassword(password));
        }

        /// <summary>
        /// Sets account password.
        /// </summary>
        /// <param name="password">New password.</param>
        public void SetPassword(string password)
        {
            PasswordHashed = HashPassword(password);
        }

        private byte[] HashPassword(string password)
        {
            // Compute password hash (with prepended and appended salt) using SHA512 algorithm
            byte[] passwordUtf8 = Encoding.UTF8.GetBytes(Info.SaltPrepend + password + Info.SaltAppend);
            return sha512.ComputeHash(passwordUtf8);
        }
    }
}
