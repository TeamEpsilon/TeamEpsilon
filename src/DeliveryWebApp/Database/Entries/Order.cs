﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp.Database.Entries
{
    public enum OrderType
    {
        PickUpTakeoutItem,
        DropOffTakeoutItem,
        GoToLocation
    }

    public class Order
    {
        /// <summary>
        ///  Order ID.
        /// </summary>
        public Guid ID { get; set; }

        public Account Assignee { get; set; }

        /// <summary>
        /// Order type.
        /// </summary>
        public OrderType Type { get; set; }

        /// <summary>
        /// Location latitude (used only if order is GoToLocation).
        /// </summary>
        public double MapLatitude { get; set; }

        /// <summary>
        /// Location longitude (used only if order is GoToLocation).
        /// </summary>
        public double MapLongitude { get; set; }

        /// <summary>
        /// Location address (used only if order is GoToLocation).
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Item targeted (used only if order is PickUpTakeoutItem or DropOffTakeoutItem).
        /// </summary>
        public Takeout TargetedItem { get; set; }

        /// <summary>
        /// Time the order was placed.
        /// </summary>
        public DateTime PlacedTime { get; set; }

        /// <summary>
        /// Determine whether the order was completed.
        /// </summary>
        public bool IsCompleted { get; set; }

        /// <summary>
        /// Time the order was completed.
        /// </summary>
        public DateTime CompletedTime { get; set; }
    }
}
