﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace DeliveryWebApp
{
    public class Program
    {
        /// <summary>
        /// Entry method.
        /// </summary>
        /// <param name="args">Command-line arguments.</param>
        public static void Main(string[] args)
        {
            // Create, build and run webhost on entry
            CreateWebHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Creates webhost builder from Startup class.
        /// </summary>
        /// <param name="args">Command-line arguments.</param>
        /// <returns></returns>
        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            // Use Startup class as startup controller
            return WebHost.CreateDefaultBuilder(args).UseStartup<Startup>();
        }
    }
}
