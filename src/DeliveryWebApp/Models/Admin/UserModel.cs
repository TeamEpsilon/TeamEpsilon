﻿using DeliveryWebApp.Database.Entries;

namespace DeliveryWebApp.Models.Admin
{
    public class UserModel
    {
        /// <summary>
        /// Username of the user.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Email of the user.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Was the email address confirmed or not.
        /// </summary>
        public bool IsVerified { get; set; }

        /// <summary>
        /// User's account role.
        /// </summary>
        public AccountType Role { get; set; }
    }
}
