﻿namespace DeliveryWebApp.Models.Admin
{
    public enum UserRole
    {
        RegularUser = 0x1,
        DeliveryUser = 0x2,
        DispatcherUser = 0x4,
        AdminUser = 0x8,
        AnyUser = 0xf
    }

    public class UserListModel
    {
        /// <summary>
        /// Desired user roles.
        /// </summary>
        public UserRole Roles { get; set; } = UserRole.AnyUser;

        /// <summary>
        /// Desired amount.
        /// </summary>
        public int Count { get; set; } = 15;

        /// <summary>
        /// Desired skipped entries.
        /// </summary>
        public int Skip { get; set; } = 0;

        /// <summary>
        /// Search string.
        /// </summary>
        public string Search { get; set; } = "";

        /// <summary>
        /// True if user list has more (is next page required).
        /// </summary>
        public bool HasMore { get; set; } = false;
    }
}
