﻿namespace DeliveryWebApp.Models.Admin
{
    public enum RestaurantState
    {
        Closed = 0x1,
        Open = 0x2,
        Unverified = 0x4,
        All = 0x7
    }

    public class RestaurantListModel
    {
        /// <summary>
        /// Desired state.
        /// </summary>
        public RestaurantState State { get; set; } = RestaurantState.All;

        /// <summary>
        /// Desired amount.
        /// </summary>
        public int Count { get; set; } = 15;

        /// <summary>
        /// Desired skipped entries.
        /// </summary>
        public int Skip { get; set; } = 0;

        /// <summary>
        /// Search string.
        /// </summary>
        public string Search { get; set; } = "";

        /// <summary>
        /// True if restaurant list has more (is next page required).
        /// </summary>
        public bool HasMore { get; set; } = false;
    }
}
