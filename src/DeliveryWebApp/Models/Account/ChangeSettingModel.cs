﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DeliveryWebApp.Models.Account
{
    public enum ChangeSettingMode
    {
        UpdateRestaurantOwner,
        PersonalInfo,
        ChangePassword,
        ChangeEmail,
        ChangeUsername
    }

    public class ChangeSettingModel
    {
        /// <summary>
        /// Indicates what will be changed.
        /// </summary>
        public ChangeSettingMode Mode { get; set; }

        /// <summary>
        /// Updates restaurant owner field.
        /// </summary>
        public bool IsOwner { get; set; }

        /// <summary>
        /// First name (personal info).
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last name (personal info).
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Cellphone number (personal info).
        /// </summary>
        public string CellphoneNumber { get; set; }

        /// <summary>
        /// Old password (change password).
        /// </summary>
        public string OldPassword { get; set; }

        /// <summary>
        /// New password (change password).
        /// </summary>
        public string NewPassword { get; set; }

        /// <summary>
        /// New email (change email).
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// New username (change username).
        /// </summary>
        public string Username { get; set; }

        private const string _namesRegex = @"^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ðđĐ ,.'-]+$";
        private const string _cellphoneNumberRegex = @"^[0-9]{2} [0-9]{3} [0-9]{3,4}$";
        private const string _emailRegex = @"^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$";
        private const string _usernameRegex = @"^[a-zA-Z0-9_.-]*$";

        /// <summary>
        /// Checks validity of the model per selected mode.
        /// </summary>
        /// <returns>True if model is valid, false otherwise.</returns>
        public bool CheckValidity()
        {
            switch (Mode)
            {
                case ChangeSettingMode.UpdateRestaurantOwner:
                    return true;
                case ChangeSettingMode.PersonalInfo:
                    return (Regex.IsMatch(FirstName, _namesRegex)) && (Regex.IsMatch(LastName, _namesRegex)) && (Regex.IsMatch(CellphoneNumber, _cellphoneNumberRegex));
                case ChangeSettingMode.ChangePassword:
                    return (NewPassword.Length >= 8) && (NewPassword.Length <= 64);
                case ChangeSettingMode.ChangeEmail:
                    return Regex.IsMatch(Email, _emailRegex);
                case ChangeSettingMode.ChangeUsername:
                    return (Username.Length >= 5) && (Username.Length <= 30) && (Regex.IsMatch(Username, _usernameRegex));
                default:
                    break;
            }
            return false;
        }
    }
}
