﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Numerics;

namespace DeliveryWebApp.Models.Account
{
    public class AddRestaurantModel
    {
        /// <summary>
        /// Name of the restaurant.
        /// </summary>
        [Required]
        [StringLength(30, MinimumLength = 5)]
        public string Name { get; set; }

        /// <summary>
        /// Description of the restaurant.
        /// </summary>
        [StringLength(300)]
        public string Description { get; set; }

        /// <summary>
        /// Restaurant address.
        /// </summary>
        [Required]
        [StringLength(80)]
        public string Address { get; set; }

        /// <summary>
        /// Latitude coordinate of the address.
        /// </summary>
        [Required]
        [Range(45.7412, 45.846)]
        public double MapLatitude { get; set; }

        /// <summary>
        /// Logintude coordinate of the restaurant.
        /// </summary>
        [Required]
        [Range(15.8274, 16.1279)]
        public double MapLongitude { get; set; }

        /// <summary>
        /// Contact number of the restaurant.
        /// </summary>
        [Required]
        [RegularExpression(@"^[0-9]{2} [0-9]{3} [0-9]{3,4}$")]
        public string ContactNumber { get; set; }

        /// <summary>
        /// Fax number of the restaurant.
        /// </summary>
        [Required]
        [RegularExpression(@"^[0-9]{2} [0-9]{3} [0-9]{3,4}$")]
        public string FaxNumber { get; set; }

        /// <summary>
        /// OIB of the restaurant (?).
        /// </summary>
        [Required]
        [StringLength(11, MinimumLength = 11)]
        public string OIB { get; set; }

        /// <summary>
        /// IBAN of the current account I guess.
        /// </summary>
        [Required]
        [StringLength(21, MinimumLength = 21)]
        public string IBAN { get; set; }

        /// <summary>
        /// IBAN of the giro account.
        /// </summary>
        [Required]
        [StringLength(21, MinimumLength = 21)]
        public string Giro { get; set; }

        /// <summary>
        /// Does this model update existing restaurant.
        /// </summary>
        public bool UpdateExisting { get; set; } = false;

        /// <summary>
        /// Which existing restaurant does it update.
        /// </summary>
        public Guid ExistingID { get; set; } = Guid.Empty;

        public bool IsValid()
        {
            return IsOIBValid() && IsIBANValid() && IsGiroValid();
        }

        public bool IsOIBValid()
        {
            try
            {
                int[] nums = OIB.Substring(0, 10).ToCharArray().Select(i => int.Parse(i + "")).ToArray();
                int cnum = ISO7064Mod1011(nums);
                int last = int.Parse(OIB.Remove(0, 10));
                return (cnum == last);
            }
            catch
            {
            }
            return false;
        }

        public bool IsIBANValid()
        {
            return CheckIBAN(IBAN);
        }

        public bool IsGiroValid()
        {
            return CheckIBAN(Giro);
        }

        private static int ISO7064Mod1011(int[] nums)
        {
            int prev = 10;
            foreach (int num in nums)
            {
                prev += num;
                prev %= 10;
                if (prev == 0)
                {
                    prev = 10;
                }
                prev *= 2;
                prev %= 11;
            }
            return (11 - prev) % 10;
        }

        private static bool CheckIBAN(string iban)
        {
            try
            {
                if (iban.Length != 21)
                {
                    return false;
                }
                if (!iban.StartsWith("HR", StringComparison.InvariantCultureIgnoreCase))
                {
                    return false;
                }
                string nums = iban.Remove(0, 4) + "1727" + iban.Substring(2, 2);
                int cnum = ISO7064Mod9710(nums);
                return (cnum == 1);
            }
            catch
            {
            }
            return false;
        }

        private static int ISO7064Mod9710(string nums)
        {
            BigInteger bigint = BigInteger.Parse(nums);
            return (int)(bigint % 97);
        }
    }
}
