﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DeliveryWebApp.Models.Account
{
    public class AddMealModel
    {
        /// <summary>
        /// Which restaurant does it belong to.
        /// </summary>
        public Guid RestaurantID { get; set; }

        /// <summary>
        /// Meal name.
        /// </summary>
        [Required]
        [StringLength(30, MinimumLength = 5)]
        public string Name { get; set; }

        /// <summary>
        /// Meal description.
        /// </summary>
        [StringLength(300)]
        public string Description { get; set; }

        /// <summary>
        /// Meal price (HRK).
        /// </summary>
        [Required]
        [Range(1.00, 500.00)]
        public double Price { get; set; }

        /// <summary>
        /// Preparation time in minutes.
        /// </summary>
        [Required]
        [Range(1.0, 180.0)]
        public uint PrepTime { get; set; }

        /// <summary>
        /// Are we editing existing.
        /// </summary>
        public bool EditExisting { get; set; } = false;

        /// <summary>
        /// Edited ID.
        /// </summary>
        public Guid EditID { get; set; } = Guid.Empty;
    }
}
