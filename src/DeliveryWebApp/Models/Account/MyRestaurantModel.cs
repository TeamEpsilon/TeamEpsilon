﻿using System;

namespace DeliveryWebApp.Models.Account
{
    public class MyRestaurantModel
    {
        /// <summary>
        /// Restaurant ID.
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// Name of the restaurant.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Is the restaurant verified.
        /// </summary>
        public bool Verified { get; set; }

        /// <summary>
        /// Is the restaurant closed.
        /// </summary>
        public bool Closed { get; set; }

        /// <summary>
        /// Creates a new restaurant model.
        /// </summary>
        /// <param name="id">Restaurant ID.</param>
        /// <param name="name">Restaurant name.</param>
        /// <param name="verified">Is the restaurant verified.</param>
        /// <param name="closed">Is the restaurant closed currently.</param>
        public MyRestaurantModel(Guid id, string name, bool verified, bool closed)
        {
            ID = id;
            Name = name;
            Verified = verified;
            Closed = closed;
        }
    }
}
