﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DeliveryWebApp.Models.Account
{
    public class CheckoutModel
    {
        /// <summary>
        /// Address of the delivery.
        /// </summary>
        [Required]
        [StringLength(80)]
        public string Address { get; set; }

        /// <summary>
        /// Map latitude of the delivery.
        /// </summary>
        [Required]
        [Range(45.7412, 45.846)]
        public double MapLatitude { get; set; } = 45.80081;

        /// <summary>
        /// Map logitude of the delivery.
        /// </summary>
        [Required]
        [Range(15.8274, 16.1279)]
        public double MapLongitude { get; set; } = 15.97162;
    }
}
