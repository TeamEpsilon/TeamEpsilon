﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp.Models.Home
{
    public class DeveloperModel
    {
        /// <summary>
        /// Developer's first and last name.
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Developer's username on GitLab (used for linking).
        /// </summary>
        public string UsernameGitLab { get; set; }
        /// <summary>
        /// Developer's roles on this project.
        /// </summary>
        public string Roles { get; set; }

        /// <summary>
        /// Creates developer model container.
        /// </summary>
        /// <param name="fullName">Developer's first and last name.</param>
        /// <param name="usernameGitLab">Developer's username on GitLab (used for linking).</param>
        /// <param name="roles">Developer's roles on this project.</param>
        public DeveloperModel(string fullName, string usernameGitLab, string roles)
        {
            FullName = fullName;
            UsernameGitLab = usernameGitLab;
            Roles = roles;
        }
    }
}
