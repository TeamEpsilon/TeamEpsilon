﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp.Models.Home
{
    public class LoginModel
    {
        /// <summary>
        /// Account's username or email.
        /// </summary>
        [Required]
        public string UsernameOrEmail { get; set; }
        /// <summary>
        /// Account's password.
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// Google's ReCaptcha response.
        /// </summary>
        [Required]
        [FromForm(Name = "g-recaptcha-response")]
        public string ReCaptchaResponse { get; set; }

        /// <summary>
        /// Required by ASP.NET Core.
        /// </summary>
        public LoginModel()
        {
        }

        /// <summary>
        /// Creates login model container.
        /// </summary>
        /// <param name="usernameOrEmail">Account's username or email.</param>
        /// <param name="password">Account's password.</param>
        public LoginModel(string usernameOrEmail, string password)
        {
            UsernameOrEmail = usernameOrEmail;
            Password = password;
        }
    }
}
