﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp.Models.Home
{
    public class RegisterModel
    {
        /// <summary>
        /// Account's username (unique).
        /// </summary>
        [Required]
        [RegularExpression(@"^[a-zA-Z0-9_.-]*$")]
        [StringLength(30, MinimumLength = 5)]
        public string Username { get; set; }
        /// <summary>
        /// Account's email (uniqe).
        /// </summary>
        [Required]
        [RegularExpression(@"^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$")]
        public string Email { get; set; }
        /// <summary>
        /// Account's password.
        /// </summary>
        [Required]
        [StringLength(64, MinimumLength = 8)]
        public string Password { get; set; }
        /// <summary>
        /// Account's first name.
        /// </summary>
        [Required]
        [RegularExpression(@"^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ðđĐ ,.'-]+$")]
        public string FirstName { get; set; }
        /// <summary>
        /// Account's last name.
        /// </summary>
        [Required]
        [RegularExpression(@"^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ðđĐ ,.'-]+$")]
        public string LastName { get; set; }
        /// <summary>
        /// Account's cellphone number.
        /// </summary>
        [Required]
        [RegularExpression(@"^[0-9]{2} [0-9]{3} [0-9]{3,4}$")]
        public string CellphoneNumber { get; set; }

        /// <summary>
        /// Google's ReCaptcha response.
        /// </summary>
        [Required]
        [FromForm(Name = "g-recaptcha-response")]
        public string ReCaptchaResponse { get; set; }

        /// <summary>
        /// Required by ASP.NET Core.
        /// </summary>
        public RegisterModel()
        {
        }

        /// <summary>
        /// Creates register model container.
        /// </summary>
        /// <param name="username">Account's username (unique).</param>
        /// <param name="email">Account's email (unique).</param>
        /// <param name="password">Account's password.</param>
        /// <param name="firstName">Account's first name.</param>
        /// <param name="lastName">Account's last name.</param>
        /// <param name="cellphoneNumber">Account's cellphone number.</param>
        public RegisterModel(string username, string email, string password, string firstName, string lastName, string cellphoneNumber)
        {
            Username = username;
            Email = email;
            Password = password;
            FirstName = firstName;
            LastName = lastName;
            CellphoneNumber = cellphoneNumber;
        }
    }
}
