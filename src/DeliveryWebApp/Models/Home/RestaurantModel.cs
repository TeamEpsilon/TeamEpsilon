﻿using System;

namespace DeliveryWebApp.Models.Home
{
    public class RestaurantModel
    {
        /// <summary>
        /// Restaurant ID.
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// Restaurant name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Restaurant description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Restaurant picture.
        /// </summary>
        public string PictureURL { get; set; }
    }
}
