﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DeliveryWebApp.Database;
using DeliveryWebApp.Database.Entries;
using DeliveryWebApp.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SendGrid;

namespace DeliveryWebApp
{
    public class Startup
    {
        private IConfiguration _configuration;

        /// <summary>
        /// Initialize startup.
        /// </summary>
        /// <param name="configuration">Configuration dependency.</param>
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
            SqlDatabase.ConnectionString = _configuration["ConnectionStrings:DefaultConnection"];
        }

        /// <summary>
        /// Configure services.
        /// </summary>
        /// <param name="services">Services collection.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Add model-view-controller service
            services.AddMvc();

            // Add HTTP content accessor service (for reading request cookies at CSHTML level)
            services.AddHttpContextAccessor();

            // Add HttpClientFactory service
            services.AddHttpClient();

            // Add ReCaptcha service
            services.AddTransient<IReCaptcha, ReCaptcha>();

            // Add SQL database and repository service
            services.AddTransient<SqlDatabase>();
            services.AddTransient<ISqlRepository, SqlRepository>();

            // Add SendGrid client and mailer bot
            services.AddTransient<ISendGridClient, SendGridClient>(s => new SendGridClient(_configuration["SendGrid:ApiKey"]));
            services.AddTransient<IMailerBot, MailerBot>();

            // Add AzureStorage service
            services.AddTransient<IStorage, Storage>();
        }

        /// <summary>
        /// Configure server.
        /// </summary>
        /// <param name="app">App builder dependency.</param>
        /// <param name="env">Environment dependency.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, SqlDatabase sqlDatabase)
        {
            // Enable more detailed exceptions if app is in Development stage
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Create database if it doesn't exist already
            sqlDatabase.Database.EnsureCreated();

            // Enable serving .webmanifest static files
            FileExtensionContentTypeProvider provider = new FileExtensionContentTypeProvider();
            provider.Mappings[".webmanifest"] = "application/manifest+json";
            StaticFileOptions options = new StaticFileOptions();
            options.ContentTypeProvider = provider;

            // Serve static files
            app.UseStaticFiles(options);

            // Enable default MVC route
            app.UseMvcWithDefaultRoute();
        }
    }
}
