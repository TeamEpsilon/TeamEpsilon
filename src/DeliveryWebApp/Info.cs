﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp
{
    public static class Info
    {
        /// <summary>
        /// Webpage title.
        /// </summary>
        public const string Title = @"Fina i brza hrana";

        /// <summary>
        /// Website URL.
        /// </summary>
        public const string Website = @"https://finaibrzahrana.azurewebsites.net/";

        /// <summary>
        /// Prepend salt value.
        /// </summary>
        public const string SaltPrepend = @"lYw8OTV3439W";

        /// <summary>
        /// Append salt value.
        /// </summary>
        public const string SaltAppend = @"JZe6AwVW0pnV";

        /// <summary>
        /// Mailer bot's email address.
        /// </summary>
        public const string MailerBotEmail = @"mailerbot@finaibrzahrana.azurewebsites.net";

        /// <summary>
        /// Google's ReCaptcha public (site) key.
        /// </summary>
        public const string ReCaptchaSiteKey = @"6LfP23UUAAAAAIfMV7qx_FrhFNgLHpLbKhSXz0XP";

        /// <summary>
        /// Duration (in seconds) of how long will the access token be valid after last renewal.
        /// </summary>
        public const UInt32 AccessTokenValidityDuration = 3 * 60 * 60; // 3 hours

        /// <summary>
        /// Duration (in seconds) of how long will the change email request be valid.
        /// </summary>
        public const UInt32 ChangeEmailRequestDuration = 3 * 60 * 60; // 3 hours
    }
}
