﻿var AddRestaurant_locationPicker;

function AccountAddRestaurant_onLoad() {
    // Call base load event handler
    Layout_onLoadBody();

    // Get location picker widget
    AddRestaurant_locationPicker = $('.location-picker').locationPicker({
        locationChanged: function (data) {
            $('#MapLatitude').val(data.location.lat);
            $('#MapLongitude').val(data.location.long);
        },
        init: { current_location: false }
    });
    
    // Initialize location to FER
    AddRestaurant_locationPicker.setLocation($('#MapLatitude').val(), $('#MapLongitude').val());
}

function AccountAddRestaurant_onFocusOutContactNumber() {
    // Get user's desired cellphone number and clear all whitespaces and dashes in it
    let desiredCellphoneNumber = $('#ContactNumber').val().replace(/[\s-]/g, '');
    $('#ContactNumber').val(desiredCellphoneNumber);

    // Clear possible prefixes like country dialing code
    if (desiredCellphoneNumber.startsWith('385')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(3);
    } else if (desiredCellphoneNumber.startsWith('+385')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(4);
    } else if (desiredCellphoneNumber.startsWith('00385')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(5);
    } else if (desiredCellphoneNumber.startsWith('0')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(1);
    }

    // Format user's desired cellphone number with accepted formatting
    desiredCellphoneNumber = desiredCellphoneNumber.substring(0, 2) + ' ' + desiredCellphoneNumber.substring(2, 5) + ' ' + desiredCellphoneNumber.substring(5);

    // Check cellphone number validity using regex
    if (Register_cellphoneNumberValidRegex.test(desiredCellphoneNumber)) {
        // Update cellphone number input field with accepted formatting
        $('#ContactNumber').val(desiredCellphoneNumber);
    }
}

function AccountAddRestaurant_onFocusOutFaxNumber() {
    // Get user's desired cellphone number and clear all whitespaces and dashes in it
    let desiredCellphoneNumber = $('#FaxNumber').val().replace(/[\s-]/g, '');
    $('#FaxNumber').val(desiredCellphoneNumber);

    // Clear possible prefixes like country dialing code
    if (desiredCellphoneNumber.startsWith('385')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(3);
    } else if (desiredCellphoneNumber.startsWith('+385')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(4);
    } else if (desiredCellphoneNumber.startsWith('00385')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(5);
    } else if (desiredCellphoneNumber.startsWith('0')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(1);
    }

    // Format user's desired cellphone number with accepted formatting
    desiredCellphoneNumber = desiredCellphoneNumber.substring(0, 2) + ' ' + desiredCellphoneNumber.substring(2, 5) + ' ' + desiredCellphoneNumber.substring(5);

    // Check cellphone number validity using regex
    if (Register_cellphoneNumberValidRegex.test(desiredCellphoneNumber)) {
        // Update cellphone number input field with accepted formatting
        $('#FaxNumber').val(desiredCellphoneNumber);
    }
}
