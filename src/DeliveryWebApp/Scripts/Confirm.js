﻿function Confirm_onKeyUpVerificationCode() {
    // Enable submit button if verification code is 5 characters long
    if ($('#id_code').val().length == 5) {
        $('#id_submit').removeAttr('disabled');
    } else {
        $('#id_submit').attr('disabled', true);
    }
}

function Confirm_requestNewEmail() {
    // Attempt to resend an verification code
    $('#id_not_sent').html('Slanje novog verifikacijskog emaila...');
    $.get('Resend?email=' + encodeURI($('#id_email').val()), function (response) {
        // Delay for 500 milliseconds
        setTimeout(function () {
            if (response.success) {
                $('#id_not_sent').html('Novi verifikacijski email je poslan.');
            } else {
                $('#id_not_sent').html('Nije moguće poslati novi verifikacijski email.').css('color', 'red');
            }
        }, 500);
    });
}
