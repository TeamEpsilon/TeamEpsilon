﻿// Randomize currently active logo index
var Layout_currentIndex = Math.floor(15 * Math.random());

function Layout_onLoadBody() {
    // Remove this event callback
    $('#id_logo').removeAttr('onload');
    // Attempt to lock mobile browser orientation to portrait
    let lockOrientation = screen.lockOrientation || screen.mozLockOrientation || screen.msLockOrientation;
    if (lockOrientation != null) {
        lockOrientation('portrait');
    }
    // Initiate logo changer
    Layout_logoChanger();
}

function Layout_logoChanger() {
    // Get previous logo DOM object
    let previousLogo = $('#id_logo_' + ((Layout_currentIndex >= 10) ? (Layout_currentIndex.toString()) : ('0' + Layout_currentIndex.toString())));
    // Get current logo DOM object
    Layout_currentIndex = (Layout_currentIndex + 1) % 15;
    let currentLogo = $('#id_logo_' + ((Layout_currentIndex >= 10) ? (Layout_currentIndex.toString()) : ('0' + Layout_currentIndex.toString())));
    // Animate logo transitions and repeat
    previousLogo.animate({width: '0%', height: '0%'}, 250, 'swing',
        function () {
            previousLogo.addClass("d-none");
            currentLogo.removeClass("d-none");
            currentLogo.animate({width: '100%', height: '100%'}, 250, 'swing',
                function () {
                    setTimeout(Layout_logoChanger, 5000);
                })
        }
    );
}
