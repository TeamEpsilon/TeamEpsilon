﻿// Validity of ReCaptcha
var Login_validReCaptcha = false;

function Login_onReCaptchaSuccess() {
    // Validate form because ReCaptcha succeeded
    Login_validReCaptcha = true;
    Login_validateForm();
}

function Login_onReCaptchaFailed() {
    // Invalidate form because ReCaptcha failed
    Login_validReCaptcha = false;
    Login_validateForm();
}

function Login_validateForm() {
    // Enable submit button only if ReCaptcha was completed
    if (Login_validReCaptcha) {
        $('#id_submit').removeAttr('disabled');
    } else {
        $('#id_submit').attr('disabled', true);
    }
}
