﻿// Validity of each form's input field
var Register_validUsername = false;
var Register_validEmail = false;
var Register_validPassword = false;
var Register_validPasswordRepeat = false;
var Register_validFullName = false;
var Register_validCellphoneNumber = false;
var Register_validReCaptcha = false;

// Form's input field constants
var Register_usernameValidChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_.-';
var Register_usernameMinLength = 5;
var Register_usernameMaxLength = 30;
var Register_emailValidRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var Register_passwordMinLength = 8;
var Register_passwordMaxLength = 64;
var Register_fullNameValidRegex = /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ðđĐ ,.'-]+$/u;
var Register_cellphoneNumberValidRegex = /^[0-9]{2} [0-9]{3} [0-9]{3,4}$/;

// Previously valid input fields
var Register_previousValidUsername = null;
var Register_previousValidEmail = null;

// Full name input field's focus flags
var Register_focusedFirstName = false;
var Register_focusedLastName = false;

function Register_onLoad() {
    // Call base load event handler
    Layout_onLoadBody();
    // Check username input if it is not blank
    if ($('#Username').val().length > 0) {
        Register_onFocusOutUsername();
    }
    // Check email input if it is not blank
    if ($('#Email').val().length > 0) {
        Register_onFocusOutEmail();
    }
    // Check first and last name input if it is not blank
    if (($('#FirstName').val().length > 0) && ($('#LastName').val().length > 0)) {
        Register_onFocusOutFullName();
    }
    // Check cellphone number input if it is not blank
    if ($('#CellphoneNumber').val().length > 0) {
        Register_onFocusOutCellphoneNumber();
    }
}

function Register_showMessage(message, show) {
    // Show slide in or slide out animation for message DOM object
    if (show) {
        message.slideDown(250, 'swing');
    } else {
        message.slideUp(250, 'swing');
    }
}

function Register_invalidateForm() {
    // Disable submit button
    $('#id_submit').attr('disabled', true);
}

function Register_validateForm() {
    // Enable submit button only if all of the form's input fields are valid
    if ((Register_validUsername) && (Register_validEmail) && (Register_validPassword) && (Register_validPasswordRepeat) && (Register_validFullName) && (Register_validCellphoneNumber) && (Register_validReCaptcha)) {
        $('#id_submit').removeAttr('disabled');
    } else {
        Register_invalidateForm();
    }
}

function Register_onFocusUsername() {
    // Invalidate form, set username invalid and hide all username messages
    Register_invalidateForm();
    Register_validUsername = false;
    Register_showMessage($('#id_username_success'), false);
    Register_showMessage($('#id_username_checking'), false);
    Register_showMessage($('#id_username_failure'), false);
    Register_showMessage($('#id_username_invalid'), false);
    Register_showMessage($('#id_username_tooshort'), false);
    Register_showMessage($('#id_username_toolong'), false);
}

function Register_onFocusOutUsername() {
    // Get user's desired username and clear all leading and trailing whitespaces in it
    let desiredUsername = $('#Username').val().trim();
    $('#Username').val(desiredUsername);
    // If username was already previously proven valid, don't check again
    if (Register_previousValidUsername == desiredUsername) {
        Register_showMessage($('#id_username_success'), true);
        Register_validUsername = true;
        Register_validateForm();
        return;
    }
    Register_previousValidUsername = null;
    // All characters in username must be valid
    for (let i = 0; i < desiredUsername.length; i++) {
        if (!Register_usernameValidChars.includes(desiredUsername.charAt(i))) {
            let illegalChar = desiredUsername.charAt(i);
            if ((illegalChar == ' ') || (illegalChar == '\t') || (illegalChar == '\r') || (illegalChar == '\n')) {
                illegalChar = 'praznine';
            }
            $('#id_username_illegal_char').html(illegalChar);
            Register_showMessage($('#id_username_invalid'), true);
            Register_validUsername = false;
            Register_invalidateForm();
            return;
        }
    }
    // Username length cannot be shorter that minimum username length
    if (desiredUsername.length < Register_usernameMinLength) {
        Register_showMessage($('#id_username_tooshort'), true);
        Register_validUsername = false;
        Register_invalidateForm();
        return;
    }
    // Username length cannot be longer than maximum username length
    if (desiredUsername.length > Register_usernameMaxLength) {
        Register_showMessage($('#id_username_toolong'), true);
        Register_validUsername = false;
        Register_invalidateForm();
        return;
    }
    // Disable username field and show checking username message
    $('#Username').attr('disabled', true);
    Register_showMessage($('#id_username_checking'), true);
    Register_validUsername = false;
    Register_invalidateForm();
    // Delay for 500 milliseconds and start a GET request to check username availability
    setTimeout(function () {
            $.get('CheckUsername?username=' + encodeURIComponent(desiredUsername), function (response) {
                // Enable username field and hide checking username message
                $('#Username').removeAttr('disabled');
                Register_showMessage($('#id_username_checking'), false);
                // Delay for 300 milliseconds and proceed by validating or invalidating form depending on username availability
                setTimeout(function () {
                    if (response.available) {
                        Register_showMessage($('#id_username_success'), true);
                        Register_validUsername = true;
                        Register_validateForm();
                        Register_previousValidUsername = desiredUsername;
                    } else {
                        Register_showMessage($('#id_username_failure'), true);
                        Register_validUsername = false;
                        Register_invalidateForm();
                    }
                }, 300);
            });
    }, 500);
}

function Register_onFocusEmail() {
    // Invalidate form, set email invalid and hide all email messages
    Register_invalidateForm();
    Register_validEmail = false;
    Register_showMessage($('#id_email_success'), false);
    Register_showMessage($('#id_email_checking'), false);
    Register_showMessage($('#id_email_failure'), false);
    Register_showMessage($('#id_email_invalid'), false);
}

function Register_onFocusOutEmail() {
    // Get user's desired email and clear all leading and trailing whitespaces in it
    let desiredEmail = $('#Email').val().trim();
    $('#Email').val(desiredEmail);
    // If email was already previously proven valid, don't check again
    if (Register_previousValidEmail == desiredEmail) {
        Register_showMessage($('#id_email_success'), true);
        Register_validEmail = true;
        Register_validateForm();
        return;
    }
    Register_previousValidEmail = null;
    // Check email validity using regex
    if (!Register_emailValidRegex.test(desiredEmail)) {
        Register_showMessage($('#id_email_invalid'), true);
        Register_validEmail = false;
        Register_invalidateForm();
        return;
    }
    // Disable email field and show checking email message
    $('#Email').attr('disabled', true);
    Register_showMessage($('#id_email_checking'), true);
    Register_validEmail = false;
    Register_invalidateForm();
    // Delay for 500 milliseconds and start a GET request to check email availability
    setTimeout(function () {
        $.get('CheckEmail?email=' + encodeURIComponent(desiredEmail), function (response) {
            // Enable email field and hide checking email message
            $('#Email').removeAttr('disabled');
            Register_showMessage($('#id_email_checking'), false);
            // Delay for 300 milliseconds and proceed by validating or invalidating form depending on email availability
            setTimeout(function () {
                if (response.available) {
                    Register_showMessage($('#id_email_success'), true);
                    Register_validEmail = true;
                    Register_validateForm();
                    Register_previousValidEmail = desiredEmail;
                } else {
                    Register_showMessage($('#id_email_failure'), true);
                    Register_validEmail = false;
                    Register_invalidateForm();
                }
            }, 300);
        });
    }, 500);
}

function Register_onFocusPassword() {
    // Invalidate form, set password invalid and hide all password messages
    Register_invalidateForm();
    Register_validPassword = false;
    Register_showMessage($('#id_password_success'), false);
    Register_showMessage($('#id_password_tooshort'), false);
    Register_showMessage($('#id_password_toolong'), false);
}

function Register_onFocusOutPassword() {
    // Get user's desired password
    let desiredPassword = $('#Password').val();
    // Password length cannot be shorter that minimum password length
    if (desiredPassword.length < Register_passwordMinLength) {
        Register_showMessage($('#id_password_tooshort'), true);
        Register_validPassword = false;
        Register_invalidateForm();
        return;
    }
    // Password length cannot be longer that maximum password length
    if (desiredPassword.length > Register_passwordMaxLength) {
        Register_showMessage($('#id_password_toolong'), true);
        Register_validPassword = false;
        Register_invalidateForm();
        return;
    }
    // Validate password and form
    Register_showMessage($('#id_password_success'), true);
    Register_validPassword = true;
    Register_validateForm();
}

function Register_onKeyDownPassword() {
    // Reset password repeat field if password field changes (ignore tab and return keys)
    if ((event.which != 9) && (event.which != 13)) {
        $('#PasswordRepeat').val('');
        Register_onFocusPasswordRepeat();
    }
}

function Register_onFocusPasswordRepeat() {
    // Invalidate form, set password repeat invalid and hide all password repeat messages
    Register_invalidateForm();
    Register_validPasswordRepeat = false;
    Register_showMessage($('#id_password_repeat_success'), false);
    Register_showMessage($('#id_password_repeat_invalid'), false);
}

function Register_onFocusOutPasswordRepeat() {
    // If password is not valid, do not validate repeated password
    if (!Register_validPassword) {
        return;
    }
    // Get user's desired and repeated password
    let desiredPassword = $('#Password').val();
    let repeatedPassword = $('#PasswordRepeat').val();
    if (desiredPassword != repeatedPassword) {
        Register_showMessage($('#id_password_repeat_invalid'), true);
        Register_validPasswordRepeat = false;
        Register_invalidateForm();
        return;
    }
    // Validate password repeat and form
    Register_showMessage($('#id_password_repeat_success'), true);
    Register_validPasswordRepeat = true;
    Register_validateForm();
}

function Register_onFocusFullName() {
    // Invalidate form, set full name invalid and hide all full name messages
    Register_invalidateForm();
    Register_validFullName = false;
    Register_showMessage($('#id_full_name_success'), false);
    Register_showMessage($('#id_full_name_invalid'), false);
}

function Register_onFocusOutFullName() {
    // Delay function by 100 milliseconds
    setTimeout(function () {
        // If first of last name input field is focused, do not display any full name messages
        if ((Register_focusedFirstName) || (Register_focusedLastName)) {
            return;
        }
        // Get user's desired full name and clear all leading and trailing whitespaces in it
        let desiredFirstName = $('#FirstName').val().trim();
        $('#FirstName').val(desiredFirstName);
        let desiredLastName = $('#LastName').val().trim();
        $('#LastName').val(desiredLastName);
        // Both first and last name must be entered
        if ((desiredFirstName.length <= 0) || (desiredLastName.length <= 0)) {
            Register_showMessage($('#id_full_name_invalid'), true);
            Register_validFullName = false;
            Register_invalidateForm();
            return;
        }
        // Check first and last name validity using regex
        if ((!Register_fullNameValidRegex.test(desiredFirstName)) || (!Register_fullNameValidRegex.test(desiredLastName))) {
            Register_showMessage($('#id_full_name_invalid'), true);
            Register_validFullName = false;
            Register_invalidateForm();
            return;
        }
        // Validate full name and form
        Register_showMessage($('#id_full_name_success'), true);
        Register_validFullName = true;
        Register_validateForm();
    }, 100);
}

function Register_onFocusCellphoneNumber() {
    // Invalidate form, set cellphone number invalid and hide all cellphone number messages
    Register_invalidateForm();
    Register_validCellphoneNumber = false;
    Register_showMessage($('#id_cellphone_number_success'), false);
    Register_showMessage($('#id_cellphone_number_invalid'), false);
}

function Register_onFocusOutCellphoneNumber() {
    // Get user's desired cellphone number and clear all whitespaces and dashes in it
    let desiredCellphoneNumber = $('#CellphoneNumber').val().replace(/[\s-]/g, '');
    $('#CellphoneNumber').val(desiredCellphoneNumber);
    // Clear possible prefixes like country dialing code
    if (desiredCellphoneNumber.startsWith('385')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(3);
    } else if (desiredCellphoneNumber.startsWith('+385')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(4);
    } else if (desiredCellphoneNumber.startsWith('00385')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(5);
    } else if (desiredCellphoneNumber.startsWith('0')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(1);
    }
    // Format user's desired cellphone number with accepted formatting
    desiredCellphoneNumber = desiredCellphoneNumber.substring(0, 2) + ' ' + desiredCellphoneNumber.substring(2, 5) + ' ' + desiredCellphoneNumber.substring(5);
    // Check cellphone number validity using regex
    if (!Register_cellphoneNumberValidRegex.test(desiredCellphoneNumber)) {
        Register_showMessage($('#id_cellphone_number_invalid'), true);
        Register_validCellphoneNumber = false;
        Register_invalidateForm();
        return;
    }
    // Update cellphone number input field with accepted formatting
    $('#CellphoneNumber').val(desiredCellphoneNumber);
    // Validate cellphone number and form
    Register_showMessage($('#id_cellphone_number_success'), true);
    Register_validCellphoneNumber = true;
    Register_validateForm();
}

function Register_onReCaptchaSuccess(response) {
    // Validate form if possible because ReCaptcha succeeded
    Register_validReCaptcha = true;
    Register_validateForm();
}

function Register_onReCaptchaFailed() {
    // Invalidate form because ReCaptcha failed
    Register_validReCaptcha = false;
    Register_invalidateForm();
}
