﻿function AccountMyRestaurant_changeDeleteRestaurantPasswordField() {
    // Enable delete restaurant button if password field is not empty
    if ($('#password').val().length > 0) {
        $('#id_delete_restaurant_button').removeAttr('disabled');
        $('#id_pseudo_delete_restaurant_button').removeAttr('disabled');
    } else {
        $('#id_delete_restaurant_button').attr('disabled', true);
        $('#id_pseudo_delete_restaurant_button').attr('disabled', true);
    }
}

function AccountMyRestaurant_deleteRestaurant() {
    // Simulate actual button click
    $('#id_delete_restaurant_button').click();
}

function AccountMyRestaurant_changeAvatar() {
    // Updates the file label on file name change
    let desiredImagePath = $('#id_avatar_field').val().trim();
    if (desiredImagePath.length > 0) {
        $('#id_image_text_var').html(desiredImagePath);
    } else {
        $('#id_image_text_var').html('Odaberite sliku.');
    }
}
