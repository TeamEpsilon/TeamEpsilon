﻿// Previously validated fields
var AccountIndex_previousValidPassword = null;
var AccountIndex_previousValidEmail = null;
var AccountIndex_previousValidUsername = null;

// Change email request ID
var AccountIndex_changeRequestId = null;

function AccountIndex_onLoad() {
    // Call base load event handler
    Layout_onLoadBody();
    // Enable tooltips
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
    // Update checkbox if account is a restaurant owner
    AccountIndex_updateIsRestaurantOwner();
}

function AccountIndex_clearDeleteAccountPasswordField() {
    // Clear password field from delete account modal window
    $('#password').val('');
}

function AccountIndex_deleteAccount() {
    // Simulate actual button click
    $('#id_delete_account_button').click();
}

function AccountIndex_changeDeleteAccountPasswordField() {
    // Enable delete account button if password field is not empty
    if ($('#password').val().length > 0) {
        $('#id_delete_account_button').removeAttr('disabled');
        $('#id_pseudo_delete_account_button').removeAttr('disabled');
    } else {
        $('#id_delete_account_button').attr('disabled', true);
        $('#id_pseudo_delete_account_button').attr('disabled', true);
    }
}

function AccountIndex_updateIsRestaurantOwner() {
    // Set is restaurant owner checkbox accordingly
    $('#id_edit_account_enable_restaurant_panel').prop('checked', ($('#id_is_restaurant_owner_var').html().trim() == '1'));
}

function AccountIndex_saveIsRestaurantOwner() {
    // Get if checkbox is checked
    let checked = $('#id_edit_account_enable_restaurant_panel').prop('checked');
    // Attempt to change the setting
    $.post('Account/ChangeSetting', { 'Mode': 'UpdateRestaurantOwner', 'IsOwner': checked }, function (response) {
        // Check if change was successful
        if (response.success) {
            // Update form accordingly
            $('#id_is_restaurant_owner_var').html(checked ? '1' : '0');
            if (checked) {
                $('#id_my_restaurants').removeClass('d-none');
            } else {
                $('#id_my_restaurants').addClass('d-none');
            }
            // Collapse other tab in accordion
            if ($('#collapseEditAccountOther').hasClass('show')) {
                $('#id_button_edit_account_other').click();
            }
            // Show success message briefly
            AccountIndex_showSuccessMessage();
        } else {
            // Show failure message briefly
            AccountIndex_showFailureMessage();
        }
    });
}

function AccountIndex_showSuccessMessage() {
    // Hide account deletion failed and avatar change failed message
    $('#id_account_deletion_failed').addClass('d-none');
    $('#id_avatar_change_failed').addClass('d-none');
    // Shows success message for 2500 milliseconds and hides it
    $('#id_success_change_account').slideDown(250, 'swing', function () {
        setTimeout(function () {
            $('#id_success_change_account').slideUp(250, 'swing');
        }, 2500);
    });
    // Scroll to top of page
    AccountIndex_scrollToTop();
}

function AccountIndex_showFailureMessage() {
    // Hide account deletion failed and avatar change failed message
    $('#id_account_deletion_failed').addClass('d-none');
    $('#id_avatar_change_failed').addClass('d-none');
    // Shows success message for 2500 milliseconds and hides it
    $('#id_failure_change_account').slideDown(250, 'swing', function () {
        setTimeout(function () {
            $('#id_failure_change_account').slideUp(250, 'swing');
        }, 2500);
    });
    // Scroll to top of page
    AccountIndex_scrollToTop();
}

function AccountIndex_scrollToTop() {
    // Scrolls to element for 250 milliseconds
    $('html, body').animate({
        scrollTop: 0
    }, 250);
}

function AccountIndex_updatePersonalInfoFormData() {
    // Reset all fields to currently saved values
    $('#id_first_name_field').val($('#id_first_name_var').html());
    $('#id_last_name_field').val($('#id_last_name_var').html());
    $('#id_cellphone_number_field').val($('#id_cellphone_number_var').html());
    // Hide all personal info messages
    $('#id_full_name_success').slideUp(250, 'swing');
    $('#id_full_name_invalid').slideUp(250, 'swing');
    $('#id_cellphone_number_success').slideUp(250, 'swing');
    $('#id_cellphone_number_invalid').slideUp(250, 'swing');
    // Reset all focused fields
    Register_focusedFirstName = false;
    Register_focusedLastName = false;
}

function AccountIndex_savePersonalInfo() {
    // Update all personal field's formatting
    AccountIndex_onFocusOutFullName();
    AccountIndex_onFocusOutCellphoneNumber();
    // Get new personal info
    let firstName = $('#id_first_name_field').val();
    let lastName = $('#id_last_name_field').val();
    let cellphoneNumber = $('#id_cellphone_number_field').val();
    // Attempt to change the setting
    $.post('Account/ChangeSetting', { 'Mode': 'PersonalInfo', 'FirstName': firstName, 'LastName': lastName, 'CellphoneNumber': cellphoneNumber }, function (response) {
        // Check if change was successful
        if (response.success) {
            // Update form accordingly
            $('#id_first_name_var').html(firstName);
            $('#id_last_name_var').html(lastName);
            $('#id_cellphone_number_var').html(cellphoneNumber);
            $('#id_titlecard_full_name').html(firstName + ' ' + lastName);
            $('#id_titlecard_cellphone_number').html(cellphoneNumber);
            // Collapse other tab in accordion
            if ($('#collapseEditAccountPersonalInfo').hasClass('show')) {
                $('#id_button_edit_account_personal_info').click();
            }
            // Show success message briefly
            AccountIndex_showSuccessMessage();
        } else {
            // Show failure message briefly
            AccountIndex_showFailureMessage();
        }
    });
}

function AccountIndex_onFocusFullName() {
    // Hide all full name validity messages
    Register_showMessage($('#id_full_name_success'), false);
    Register_showMessage($('#id_full_name_invalid'), false);
}

function AccountIndex_onFocusOutFullName() {
    // Delay function by 100 milliseconds
    setTimeout(function () {
        // If first of last name input field is focused, do not display any full name messages
        if ((Register_focusedFirstName) || (Register_focusedLastName)) {
            return;
        }
        // Get user's desired full name and clear all leading and trailing whitespaces in it
        let desiredFirstName = $('#id_first_name_field').val().trim();
        $('#id_first_name_field').val(desiredFirstName);
        let desiredLastName = $('#id_last_name_field').val().trim();
        $('#id_last_name_field').val(desiredLastName);
        // Both first and last name must be entered
        if ((desiredFirstName.length <= 0) || (desiredLastName.length <= 0)) {
            Register_showMessage($('#id_full_name_invalid'), true);
            return;
        }
        // Check first and last name validity using regex
        if ((!Register_fullNameValidRegex.test(desiredFirstName)) || (!Register_fullNameValidRegex.test(desiredLastName))) {
            Register_showMessage($('#id_full_name_invalid'), true);
            return;
        }
        // Set full name as valid
        Register_showMessage($('#id_full_name_success'), true);
    }, 100);
}

function AccountIndex_onFocusCellphoneNumber() {
    // Hide all cellphone number messages
    Register_showMessage($('#id_cellphone_number_success'), false);
    Register_showMessage($('#id_cellphone_number_invalid'), false);
}

function AccountIndex_onFocusOutCellphoneNumber() {
    // Get user's desired cellphone number and clear all whitespaces and dashes in it
    let desiredCellphoneNumber = $('#id_cellphone_number_field').val().replace(/[\s-]/g, '');
    $('#id_cellphone_number_field').val(desiredCellphoneNumber);
    // Clear possible prefixes like country dialing code
    if (desiredCellphoneNumber.startsWith('385')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(3);
    } else if (desiredCellphoneNumber.startsWith('+385')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(4);
    } else if (desiredCellphoneNumber.startsWith('00385')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(5);
    } else if (desiredCellphoneNumber.startsWith('0')) {
        desiredCellphoneNumber = desiredCellphoneNumber.substring(1);
    }
    // Format user's desired cellphone number with accepted formatting
    desiredCellphoneNumber = desiredCellphoneNumber.substring(0, 2) + ' ' + desiredCellphoneNumber.substring(2, 5) + ' ' + desiredCellphoneNumber.substring(5);
    // Check cellphone number validity using regex
    if (!Register_cellphoneNumberValidRegex.test(desiredCellphoneNumber)) {
        Register_showMessage($('#id_cellphone_number_invalid'), true);
        return;
    }
    // Update cellphone number input field with accepted formatting
    $('#id_cellphone_number_field').val(desiredCellphoneNumber);
    // Validate cellphone number and form
    Register_showMessage($('#id_cellphone_number_success'), true);
}

function AccountIndex_onFocusCurrentPassword() {
    // Hide all password messages
    Register_showMessage($('#id_current_password_success'), false);
    Register_showMessage($('#id_current_password_checking'), false);
    Register_showMessage($('#id_current_password_failure'), false);
}

function AccountIndex_onFocusOutCurrentPassword() {
    // Get entered password value
    let currentPassword = $('#id_current_password_field').val();
    // If it was already previously validated, skip further validation
    if (AccountIndex_previousValidPassword == currentPassword) {
        Register_showMessage($('#id_current_password_success'), true);
        return;
    }
    AccountIndex_previousValidPassword = null;
    // Disable password field and show checking password message
    Register_showMessage($('#id_current_password_checking'), true);
    $('#id_current_password_field').attr('disabled', true);
    // Delay for 500 milliseconds and start a GET request to check password validity
    setTimeout(function () {
        $.post('Account/CheckPassword', { 'password': currentPassword }, function (response) {
            // Enable password field and hide checking password message
            $('#id_current_password_field').removeAttr('disabled');
            Register_showMessage($('#id_current_password_checking'), false);
            // Delay for 300 milliseconds and show appropriate message
            setTimeout(function () {
                if (response.correct) {
                    Register_showMessage($('#id_current_password_success'), true);
                    AccountIndex_previousValidPassword = currentPassword;
                } else {
                    Register_showMessage($('#id_current_password_failure'), true);
                }
            }, 300);
        });
    }, 500);
}

function AccountIndex_onFocusPassword() {
    // Hide all password messages
    Register_showMessage($('#id_password_success'), false);
    Register_showMessage($('#id_password_tooshort'), false);
    Register_showMessage($('#id_password_toolong'), false);
}

function AccountIndex_onFocusOutPassword() {
    // Get user's desired password
    let desiredPassword = $('#id_new_password_field').val();
    // Password length cannot be shorter that minimum password length
    if (desiredPassword.length < Register_passwordMinLength) {
        Register_showMessage($('#id_password_tooshort'), true);
        Register_validPassword = false;
        return;
    }
    // Password length cannot be longer that maximum password length
    if (desiredPassword.length > Register_passwordMaxLength) {
        Register_showMessage($('#id_password_toolong'), true);
        Register_validPassword = false;
        return;
    }
    // Validate password
    Register_showMessage($('#id_password_success'), true);
    Register_validPassword = true;
}

function AccountIndex_onKeyDownPassword() {
    // Reset password repeat field if password field changes (ignore tab and return keys)
    if ((event.which != 9) && (event.which != 13)) {
        $('#id_repeat_new_password_field').val('');
        AccountIndex_onFocusPasswordRepeat();
    }
}

function AccountIndex_onFocusPasswordRepeat() {
    // Hide all password repeat messages
    Register_showMessage($('#id_password_repeat_success'), false);
    Register_showMessage($('#id_password_repeat_invalid'), false);
}

function AccountIndex_onFocusOutPasswordRepeat() {
    // If password is not valid, do not validate repeated password
    if (!Register_validPassword) {
        return;
    }
    // Get user's desired and repeated password
    let desiredPassword = $('#id_new_password_field').val();
    let repeatedPassword = $('#id_repeat_new_password_field').val();
    if (desiredPassword != repeatedPassword) {
        Register_showMessage($('#id_password_repeat_invalid'), true);
        return;
    }
    // Validate password repeat
    Register_showMessage($('#id_password_repeat_success'), true);
}



function AccountIndex_updatePasswordFormData() {
    // Reset all fields to currently saved values
    $('#id_current_password_field').val('');
    $('#id_new_password_field').val('');
    $('#id_repeat_new_password_field').val('');
    // Hide all password messages
    $('#id_current_password_success').slideUp(250, 'swing');
    $('#id_current_password_checking').slideUp(250, 'swing');
    $('#id_current_password_failure').slideUp(250, 'swing');
    $('#id_password_success').slideUp(250, 'swing');
    $('#id_password_tooshort').slideUp(250, 'swing');
    $('#id_password_toolong').slideUp(250, 'swing');
    $('#id_password_repeat_success').slideUp(250, 'swing');
    $('#id_password_repeat_invalid').slideUp(250, 'swing');
    // Reset password previously validated field
    AccountIndex_previousValidPassword = null;
    // Invalidate new password field
    Register_validPassword = false;
}

function AccountIndex_savePassword() {
    // Get password data
    let currentPassword = $('#id_current_password_field').val();
    let newPassword = $('#id_new_password_field').val();
    let newPasswordRepeat = $('#id_repeat_new_password_field').val();
    // Fail if password is not repeated properly
    if (newPassword != newPasswordRepeat) {
        AccountIndex_showFailureMessage();
        return;
    }
    // Attempt to change the setting
    $.post('Account/ChangeSetting', { 'Mode': 'ChangePassword', 'OldPassword': currentPassword, 'NewPassword': newPassword }, function (response) {
        // Check if change was successful
        if (response.success) {
            // Update form accordingly
            $('#id_current_password_field').html('');
            $('#id_new_password_field').html('');
            $('#id_repeat_new_password_field').html('');
            // Collapse other tab in accordion
            if ($('#collapseEditAccountPassword').hasClass('show')) {
                $('#id_button_edit_password').click();
            }
            // Show success message briefly
            AccountIndex_showSuccessMessage();
        } else {
            // Show failure message briefly
            AccountIndex_showFailureMessage();
        }
    });
}

function AccountIndex_onFocusEmail() {
    // Hide all email messages
    Register_showMessage($('#id_email_success'), false);
    Register_showMessage($('#id_email_checking'), false);
    Register_showMessage($('#id_email_failure'), false);
    Register_showMessage($('#id_email_invalid'), false);
}

function AccountIndex_onFocusOutEmail() {
    // Get user's desired email and clear all leading and trailing whitespaces in it
    let desiredEmail = $('#id_new_email_field').val().trim();
    $('#id_new_email_field').val(desiredEmail);
    // If email already belongs to account, you don't need to check
    if (desiredEmail == $('#id_email_var').html()) {
        Register_showMessage($('#id_email_success'), true);
        return;
    }
    // If email was already previously proven valid, don't check again
    if (AccountIndex_previousValidEmail == desiredEmail) {
        Register_showMessage($('#id_email_success'), true);
        return;
    }
    AccountIndex_previousValidEmail = null;
    // Check email validity using regex
    if (!Register_emailValidRegex.test(desiredEmail)) {
        Register_showMessage($('#id_email_invalid'), true);
        return;
    }
    // Disable email field and show checking email message
    $('#id_new_email_field').attr('disabled', true);
    Register_showMessage($('#id_email_checking'), true);
    // Delay for 500 milliseconds and start a GET request to check email availability
    setTimeout(function () {
        $.get('CheckEmail?email=' + encodeURIComponent(desiredEmail), function (response) {
            // Enable email field and hide checking email message
            $('#id_new_email_field').removeAttr('disabled');
            Register_showMessage($('#id_email_checking'), false);
            // Delay for 300 milliseconds and proceed accordingly
            setTimeout(function () {
                if (response.available) {
                    Register_showMessage($('#id_email_success'), true);
                    AccountIndex_previousValidEmail = desiredEmail;
                } else {
                    Register_showMessage($('#id_email_failure'), true);
                }
            }, 300);
        });
    }, 500);
}

function AccountIndex_updateEmailFormData() {
    // Reset all fields to currently saved values
    $('#id_new_email_field').val($('#id_email_var').html());
    // Hide all email messages
    $('#id_email_success').slideUp(250, 'swing');
    $('#id_email_checking').slideUp(250, 'swing');
    $('#id_email_failure').slideUp(250, 'swing');
    $('#id_email_invalid').slideUp(250, 'swing');
    // Reset email previously validated field
    AccountIndex_previousValidEmail = null;
}

function AccountIndex_saveEmail() {
    // Attempt to parse email field
    AccountIndex_onFocusOutEmail();
    // Get password data
    let desiredEmail = $('#id_new_email_field').val();
    // If email already belongs to this account, skip changing
    if (desiredEmail == $('#id_email_var').html()) {
        // Collapse other tab in accordion
        if ($('#collapseEditAccountEmail').hasClass('show')) {
            $('#id_button_edit_email').click();
        }
        // Show success message briefly
        AccountIndex_showSuccessMessage();
        return;
    }
    // Attempt to change the setting
    $.post('Account/ChangeSetting', { 'Mode': 'ChangeEmail', 'Email': desiredEmail }, function (response) {
        // Check if change was successful
        if (response.success) {
            // Show verification form
            $('#id_target_email_var').html(desiredEmail);
            $('#id_modal_change_email').modal({ backdrop: 'static', keyboard: false });
            // Remember request ID
            AccountIndex_changeRequestId = response.requestID;
            $('#id_identification').val(AccountIndex_changeRequestId);
        } else {
            // Show failure message briefly
            AccountIndex_showFailureMessage();
        }
    });
}

function AccountIndex_changeChangeEmailCodeField() {
    // Enable confirm button if verification field has 5 chars
    if ($('#code').val().length == 5) {
        $('#id_change_email_button').removeAttr('disabled');
        $('#id_pseudo_change_email_button').removeAttr('disabled');
    } else {
        $('#id_change_email_button').attr('disabled', true);
        $('#id_pseudo_change_email_button').attr('disabled', true);
    }
}

function AccountIndex_changeEmailPseudo() {
    // Invoke click on actual change email button
    $('#id_change_email_button').click();
}

function AccountIndex_requestNewEmail() {
    // Attempt to resend an verification code
    $('#id_not_sent').html('Slanje novog verifikacijskog emaila...');
    $.post('Account/ChangeSetting', { 'Mode': 'ChangeEmail', 'Email': $('#id_new_email_field').val() }, function (response) {
        // Delay for 500 milliseconds
        setTimeout(function () {
            if (response.success) {
                $('#id_not_sent').html('Novi verifikacijski email je poslan.');
                AccountIndex_changeRequestId = response.requestID;
                $('#id_identification').val(AccountIndex_changeRequestId);
            } else {
                $('#id_not_sent').html('Nije moguće poslati novi verifikacijski email.').css('color', 'red');
            }
        }, 500);
    });
}

function AccountIndex_onChangeAvatar() {
    // Updates the file label on file name change
    let desiredImagePath = $('#id_avatar_field').val().trim();
    if (desiredImagePath.length > 0) {
        $('#id_image_text_var').html(desiredImagePath);
    } else {
        $('#id_image_text_var').html('Odaberite sliku.');
    }
}

function AccountIndex_onFocusUsername() {
    // Hide all username messages
    Register_showMessage($('#id_username_success'), false);
    Register_showMessage($('#id_username_checking'), false);
    Register_showMessage($('#id_username_failure'), false);
    Register_showMessage($('#id_username_invalid'), false);
    Register_showMessage($('#id_username_tooshort'), false);
    Register_showMessage($('#id_username_toolong'), false);
}

function AccountIndex_onFocusOutUsername() {
    // Get user's desired username and clear all leading and trailing whitespaces in it
    let desiredUsername = $('#id_new_username_field').val().trim();
    $('#id_new_username_field').val(desiredUsername);
    // If username already belongs to account, you don't need to check
    if (desiredUsername == $('#id_username_var').html()) {
        Register_showMessage($('#id_username_success'), true);
        return;
    }
    // If username was already previously proven valid, don't check again
    if (AccountIndex_previousValidUsername == desiredUsername) {
        Register_showMessage($('#id_username_success'), true);
        return;
    }
    AccountIndex_previousValidUsername = null;
    // All characters in username must be valid
    for (let i = 0; i < desiredUsername.length; i++) {
        if (!Register_usernameValidChars.includes(desiredUsername.charAt(i))) {
            let illegalChar = desiredUsername.charAt(i);
            if ((illegalChar == ' ') || (illegalChar == '\t') || (illegalChar == '\r') || (illegalChar == '\n')) {
                illegalChar = 'praznine';
            }
            $('#id_username_illegal_char').html(illegalChar);
            Register_showMessage($('#id_username_invalid'), true);
            return;
        }
    }
    // Username length cannot be shorter that minimum username length
    if (desiredUsername.length < Register_usernameMinLength) {
        Register_showMessage($('#id_username_tooshort'), true);
        return;
    }
    // Username length cannot be longer than maximum username length
    if (desiredUsername.length > Register_usernameMaxLength) {
        Register_showMessage($('#id_username_toolong'), true);
        return;
    }
    // Disable username field and show checking username message
    $('#id_new_username_field').attr('disabled', true);
    Register_showMessage($('#id_username_checking'), true);
    // Delay for 500 milliseconds and start a GET request to check username availability
    setTimeout(function () {
        $.get('CheckUsername?username=' + encodeURIComponent(desiredUsername), function (response) {
            // Enable username field and hide checking username message
            $('#id_new_username_field').removeAttr('disabled');
            Register_showMessage($('#id_username_checking'), false);
            // Delay for 300 milliseconds and proceed accordingly
            setTimeout(function () {
                if (response.available) {
                    Register_showMessage($('#id_username_success'), true);
                    AccountIndex_previousValidUsername = desiredUsername;
                } else {
                    Register_showMessage($('#id_username_failure'), true);
                }
            }, 300);
        });
    }, 500);
}

function AccountIndex_updateUsernameFormData() {
    // Reset all fields to currently saved values
    $('#id_new_username_field').val($('#id_username_var').html());
    // Hide all username messages
    $('#id_username_success').slideUp(250, 'swing');
    $('#id_username_checking').slideUp(250, 'swing');
    $('#id_username_failure').slideUp(250, 'swing');
    $('#id_username_invalid').slideUp(250, 'swing');
    $('#id_username_tooshort').slideUp(250, 'swing');
    $('#id_username_toolong').slideUp(250, 'swing');
    // Reset username previously validated field
    AccountIndex_previousValidUsername = null;
}

function AccountIndex_saveUsername() {
    // Format username
    AccountIndex_onFocusOutUsername();
    // Get password data
    let desiredUsername = $('#id_new_username_field').val();
    // Attempt to change the setting
    $.post('Account/ChangeSetting', { 'Mode': 'ChangeUsername', 'Username': desiredUsername }, function (response) {
        // Check if change was successful
        if (response.success) {
            // Update form accordingly
            $('#id_new_username_field').val(desiredUsername);
            $('#id_username_var').html(desiredUsername);
            $('#id_layout_username').html(desiredUsername);
            // Collapse other tab in accordion
            if ($('#collapseEditAccountUsername').hasClass('show')) {
                $('#id_button_edit_username').click();
            }
            // Show success message briefly
            AccountIndex_showSuccessMessage();
        } else {
            // Show failure message briefly
            AccountIndex_showFailureMessage();
        }
    });
}
