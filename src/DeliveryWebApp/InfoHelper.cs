﻿using DeliveryWebApp.Database.Entries;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryWebApp
{
    public static class InfoHelper
    {
        /// <summary>
        /// Generate page title for window.
        /// </summary>
        /// <param name="pageTitle">Original page title.</param>
        /// <returns></returns>
        public static string GetPageTitle(string pageTitle)
        {
            if (pageTitle == string.Empty)
            {
                return Info.Title;
            }
            return pageTitle + " - " + Info.Title;
        }

        /// <summary>
        /// Reads (simulated) access token object from request headers.
        /// </summary>
        /// <param name="httpRequest">Request headers.</param>
        /// <returns>Simulated access token.</returns>
        public static AccessToken ReadSimulatedAccessToken(this HttpRequest httpRequest)
        {
            try
            {
                return new AccessToken(
                    Guid.Parse(httpRequest.Cookies["fibh_AccessID"]),
                    Guid.Parse(httpRequest.Cookies["fibh_AccessKey"])
                    );
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Writes access token's ID and key to response headers.
        /// </summary>
        /// <param name="httpResponse">Response headers.</param>
        /// <param name="accessToken">Target access token</param>
        public static void WriteAccessToken(this HttpResponse httpResponse, AccessToken accessToken)
        {
            CookieOptions options = new CookieOptions();
            options.HttpOnly = true;
            options.IsEssential = true;
            options.Secure = true;
            options.SameSite = SameSiteMode.Strict;
            httpResponse.Cookies.Append("fibh_AccessID", accessToken.AccessID.ToString());
            httpResponse.Cookies.Append("fibh_AccessKey", accessToken.AccessKey.ToString());
        }

        /// <summary>
        /// Clears access token's ID and key from response headers.
        /// </summary>
        /// <param name="httpResponse">Response headers.</param>
        public static void ClearAccessToken(this HttpResponse httpResponse)
        {
            httpResponse.Cookies.Delete("fibh_AccessID");
            httpResponse.Cookies.Delete("fibh_AccessKey");
        }

        /// <summary>
        /// Resizes/crops the image.
        /// </summary>
        /// <param name="input">Input bytes.</param>
        /// <param name="width">Width of the new image.</param>
        /// <param name="height">Height of the new image.</param>
        /// <returns>JPEG data of resized image.</returns>
        public static byte[] ResizeCropImage(byte[] input, int width, int height)
        {
            Image image;
            using (MemoryStream mstr = new MemoryStream(input))
            {
                image = Bitmap.FromStream(mstr);
            }
            int preX = 0, preY = 0, newWidth = width, newHeight = height;
            if (image.Width >= image.Height)
            {
                preX = (image.Width - image.Height) / 2;
                newWidth = newHeight = image.Height;
            }
            else
            {
                preY = (image.Height - image.Width) / 2;
                newWidth = newHeight = image.Width;
            }
            Image newImage = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(newImage))
            {
                g.DrawImage(image, new Rectangle(0, 0, width, height), new Rectangle(preX, preY, newWidth, newHeight), GraphicsUnit.Pixel);
            }
            byte[] result;
            using (MemoryStream str = new MemoryStream())
            {
                newImage.Save(str, ImageFormat.Jpeg);
                result = str.ToArray();
            }
            return result;
        }
    }
}
