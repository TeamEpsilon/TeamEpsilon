﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliveryWebApp.Database;
using DeliveryWebApp.Database.Entries;
using DeliveryWebApp.Models.Admin;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryWebApp.Controllers
{
    [Route("Admin")]
    public class AdminController : Controller
    {
        private ISqlRepository _sqlRepository;

        public AdminController(ISqlRepository sqlRepository)
        {
            _sqlRepository = sqlRepository;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Index(bool? deleteRestaurantSuccess, bool? deletedAccount, bool? changedRole)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                ViewData["DeleteRestaurant"] = deleteRestaurantSuccess.HasValue;
                if (deleteRestaurantSuccess.HasValue)
                {
                    ViewData["DeleteRestaurantSuccess"] = deleteRestaurantSuccess.Value;
                }
                ViewData["DeletedAccount"] = deletedAccount.HasValue;
                if (deletedAccount.HasValue)
                {
                    ViewData["DeletedAccountSuccess"] = deletedAccount.Value;
                }
                ViewData["ChangedRole"] = changedRole.HasValue;
                if (changedRole.HasValue)
                {
                    ViewData["ChangedRoleSuccess"] = changedRole.Value;
                }
                if (account.AccountRole == AccountType.Administrator)
                {
                    return View();
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("Restaurant")]
        public async Task<IActionResult> Restaurant(Guid id)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account.AccountRole == AccountType.Administrator)
                {
                    Restaurant restaurant = await _sqlRepository.FetchRestaurant(id);
                    ViewData["Restaurant"] = restaurant;
                    ViewData["Owner"] = restaurant.Owner;
                    return View();
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("Restaurants")]
        public async Task<IActionResult> Restaurants(RestaurantListModel model)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account.AccountRole == AccountType.Administrator)
                {
                    return View(model);
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("DeleteRestaurant")]
        public async Task<IActionResult> DeleteRestaurant(Guid id)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account.AccountRole == AccountType.Administrator)
                {
                    return RedirectToAction("", new { deleteRestaurantSuccess = await _sqlRepository.DeleteRestaurant(id) });
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("ToggleOpen")]
        public async Task<IActionResult> ToggleOpen(Guid id)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account.AccountRole == AccountType.Administrator)
                {
                    await _sqlRepository.ToggleOpen(id);
                    return RedirectToAction("Restaurant", new { id = id });
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("ToggleVerify")]
        public async Task<IActionResult> ToggleVerify(Guid id)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account.AccountRole == AccountType.Administrator)
                {
                    await _sqlRepository.ToggleVerify(id);
                    return RedirectToAction("Restaurant", new { id = id });
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpPost]
        [Route("ChangeRole")]
        public async Task<IActionResult> ChangeRole(string username, AccountType role)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account myAccount = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (myAccount.AccountRole == AccountType.Administrator)
                {
                    if (await _sqlRepository.ChangeUserRole(username, role))
                    {
                        return RedirectToAction("User", "Home", new { username = username });
                    }
                    else
                    {
                        return RedirectToAction("", "Admin", new { changedRole = false });
                    }
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("DeleteUser")]
        public async Task<IActionResult> DeleteUser(string username)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account myAccount = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (myAccount.AccountRole == AccountType.Administrator)
                {
                    Account targetAccount = await _sqlRepository.GetAccountAsync(username);
                    if (targetAccount != null)
                    {
                        return RedirectToAction("", new { deletedAccount = await _sqlRepository.DeleteAccount(targetAccount) });
                    }
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("Users")]
        public async Task<IActionResult> Users(UserListModel model)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account.AccountRole == AccountType.Administrator)
                {
                    return View(model);
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("VerifyEmail")]
        public async Task<IActionResult> VerifyEmail(string username)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account.AccountRole == AccountType.Administrator)
                {
                    if (await _sqlRepository.VerifyEmailAdminOnly(username))
                    {
                        return RedirectToAction("User", "Home", new { username = username });
                    }
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }
    }
}
