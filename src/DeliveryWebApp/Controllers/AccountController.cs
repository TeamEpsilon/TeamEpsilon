﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DeliveryWebApp.Database;
using DeliveryWebApp.Database.Entries;
using DeliveryWebApp.Models.Account;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace DeliveryWebApp.Controllers
{
    [Route("Account")]
    public class AccountController : Controller
    {
        private ISqlRepository _sqlRepository;

        public AccountController(ISqlRepository sqlRepository)
        {
            _sqlRepository = sqlRepository;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Index(bool? failedDeletion, bool? failedAvatarChange)
        {
            if (failedDeletion.HasValue)
            {
                ViewData["FailedDeletion"] = failedDeletion.Value;
            }
            if (failedAvatarChange.HasValue)
            {
                ViewData["FailedAvatarChange"] = failedAvatarChange.Value;
            }
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                ViewData["ProfilePictureURL"] = account.ProfilePictureURL;
                ViewData["Username"] = account.Username;
                ViewData["Email"] = account.Email;
                ViewData["FirstName"] = account.FirstName;
                ViewData["LastName"] = account.LastName;
                ViewData["CellphoneNumber"] = account.CellphoneNumber;
                ViewData["IsRestaurantOwner"] = account.IsRestaurantOwner;
                return View();
            }
            catch
            {
                return RedirectToAction("", "");
            }
        }

        [HttpPost]
        [Route("ChangeSetting")]
        public async Task<IActionResult> ChangeSetting(ChangeSettingModel changeSetting)
        {
            try
            {
                if (changeSetting.CheckValidity())
                {
                    AccessToken accessToken = Request.ReadSimulatedAccessToken();
                    Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                    if (account != null)
                    {
                        switch (changeSetting.Mode)
                        {
                            case ChangeSettingMode.UpdateRestaurantOwner:
                                return Json(new { Success = await _sqlRepository.UpdateRestaurantOwner(account.ID, changeSetting.IsOwner) });
                            case ChangeSettingMode.PersonalInfo:
                                return Json(new { Success = await _sqlRepository.UpdatePersonalInfo(account.ID, changeSetting.FirstName, changeSetting.LastName, changeSetting.CellphoneNumber) });
                            case ChangeSettingMode.ChangePassword:
                                return Json(new { Success = await _sqlRepository.ChangePassword(account.ID, changeSetting.OldPassword, changeSetting.NewPassword) });
                            case ChangeSettingMode.ChangeEmail:
                                Guid requestId = await _sqlRepository.RequestChangeEmail(account.ID, changeSetting.Email);
                                if (requestId != Guid.Empty)
                                {
                                    return Json(new { Success = true, RequestID = requestId.ToString() });
                                }
                                break;
                            case ChangeSettingMode.ChangeUsername:
                                return Json(new { Success = await _sqlRepository.ChangeUsername(account.ID, changeSetting.Username) });
                            default:
                                break;
                        }
                    }
                }
            }
            catch
            {
            }
            return Json(new { Success = false });
        }

        [HttpPost]
        [Route("DeleteAccount")]
        public async Task<IActionResult> DeleteAccount(string password)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account.CheckPassword(password))
                {
                    if (await _sqlRepository.DeleteAccount(account))
                    {
                        Response.ClearAccessToken();
                        return RedirectToAction("", "", new { CompletedDeletion = true });
                    }
                }
            }
            catch
            {
            }
            return RedirectToAction("", new { FailedDeletion = true });
        }

        [HttpPost]
        [Route("CheckPassword")]
        public async Task<IActionResult> CheckPassword(string password)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                return Json(new { Correct = account.CheckPassword(password) });
            }
            catch
            {
            }
            return Json(new { Correct = false });
        }

        [HttpGet]
        [Route("Confirm")]
        public async Task<IActionResult> Confirm(Guid id, string code)
        {
            try
            {
                if (await _sqlRepository.AttemptVerifyEmailChangeRequest(id, code))
                {
                    return RedirectToAction("", "", new { CompleteEmailChange = true });
                }
            }
            catch
            {
            }
            return RedirectToAction("", "", new { FailEmailChange = true });
        }

        [HttpPost]
        [Route("UploadAvatar")]
        public async Task<IActionResult> UploadAvatar(IFormFile avatar)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    byte[] data;
                    using (MemoryStream stream = new MemoryStream())
                    {
                        await avatar.CopyToAsync(stream);
                        data = stream.ToArray();
                    }
                    if (await _sqlRepository.ChangeAvatar(account.ID, data))
                    {
                        return RedirectToAction("");
                    }
                }
            }
            catch
            {
            }
            return RedirectToAction("", new { FailedAvatarChange = true });
        }

        [HttpGet]
        [Route("MyRestaurants")]
        public async Task<IActionResult> MyRestaurants(bool? addSuccess, bool? wasEdit, bool? wasDeleted, bool? deleteSuccess)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    ViewData["AddSuccess"] = addSuccess.HasValue && addSuccess.Value;
                    ViewData["WasEdit"] = wasEdit.HasValue && wasEdit.Value;
                    ViewData["WasDeleted"] = wasDeleted.HasValue && wasDeleted.Value;
                    ViewData["DeleteSuccess"] = deleteSuccess.HasValue && deleteSuccess.Value;
                    ViewData["MyRestaurants"] = await _sqlRepository.FetchAccountRestaurants(account.ID);
                    return View();
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("AddRestaurant")]
        public async Task<IActionResult> AddRestaurant()
        {
            return await AddRestaurant(null);
        }

        [HttpPost]
        [Route("AddRestaurant")]
        public async Task<IActionResult> AddRestaurant(AddRestaurantModel model)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    if (model == null)
                    {
                        model = new AddRestaurantModel()
                        {
                            MapLatitude = 45.80081,
                            MapLongitude = 15.97162
                        };
                        return View(model);
                    }
                    else if ((ModelState.IsValid) && (model.IsValid()))
                    {
                        if (await _sqlRepository.AttemptAddEditRestaurant(account.ID, model))
                        {
                            return RedirectToAction("MyRestaurants", new { addSuccess = true, wasEdit = model.UpdateExisting });
                        }
                        else
                        {
                            ViewData["UnknownError"] = true;
                            return View(model);
                        }
                    }
                    ViewData["InvalidName"] = (ModelState["Name"].ValidationState != ModelValidationState.Valid);
                    ViewData["InvalidDescription"] = (ModelState["Description"].ValidationState != ModelValidationState.Valid);
                    ViewData["InvalidAddress"] = (ModelState["Address"].ValidationState != ModelValidationState.Valid);
                    ViewData["InvalidLocation"] = (ModelState["MapLatitude"].ValidationState != ModelValidationState.Valid) || (ModelState["MapLongitude"].ValidationState != ModelValidationState.Valid);
                    ViewData["InvalidPhone"] = (ModelState["ContactNumber"].ValidationState != ModelValidationState.Valid);
                    ViewData["InvalidFax"] = (ModelState["FaxNumber"].ValidationState != ModelValidationState.Valid);
                    ViewData["InvalidOIB"] = !model.IsOIBValid();
                    ViewData["InvalidIBAN"] = !model.IsIBANValid();
                    ViewData["InvalidGiro"] = !model.IsGiroValid();
                    return View(model);
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("MyRestaurant")]
        public async Task<IActionResult> MyRestaurant(Guid id, bool? wasOpened, bool? wasClosed)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    Restaurant restaurant = await _sqlRepository.FetchRestaurantIfOwned(account.ID, id);
                    if (restaurant != null)
                    {
                        ViewData["WasOpened"] = wasOpened.HasValue && wasOpened.Value;
                        ViewData["WasClosed"] = wasClosed.HasValue && wasClosed.Value;
                        ViewData["ID"] = restaurant.ID;
                        ViewData["Name"] = restaurant.Name;
                        ViewData["Description"] = restaurant.Description;
                        ViewData["PictureURL"] = restaurant.PictureURL;
                        ViewData["Verified"] = restaurant.IsVerified;
                        ViewData["Closed"] = !restaurant.IsOpen;
                        ViewData["AmIAdmin"] = (account.AccountRole == AccountType.Administrator);
                        return View();
                    }
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("EditRestaurant")]
        public async Task<IActionResult> EditRestaurant(Guid id)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    Restaurant restaurant = await _sqlRepository.FetchRestaurantIfOwned(account.ID, id);
                    if (restaurant != null)
                    {
                        AddRestaurantModel adm = new AddRestaurantModel()
                        {
                            Name = restaurant.Name,
                            Description = restaurant.Description,
                            Address = restaurant.Address,
                            MapLatitude = restaurant.MapLatitude,
                            MapLongitude = restaurant.MapLongitude,
                            ContactNumber = restaurant.ContactNumber,
                            FaxNumber = restaurant.FaxNumber,
                            OIB = restaurant.OIB,
                            IBAN = restaurant.IBAN,
                            Giro = restaurant.Giro,
                            ExistingID = restaurant.ID,
                            UpdateExisting = true
                        };
                        return View("AddRestaurant", adm);
                    }
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpPost]
        [Route("DeleteRestaurant")]
        public async Task<IActionResult> DeleteRestaurant(Guid id, string password)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    return RedirectToAction("MyRestaurants", new { wasDeleted = true, deleteSuccess = await _sqlRepository.DeleteRestaurantIfOwned(account.ID, id, password) });
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("ToggleOpen")]
        public async Task<IActionResult> ToggleOpen(Guid id)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    OpenState os = await _sqlRepository.ToggleOpenIfOwned(account.ID, id);
                    if (os.Success)
                    {
                        return RedirectToAction("MyRestaurant", new { id = id, wasOpened = os.IsOpen, wasClosed = !os.IsOpen });
                    }
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpPost]
        [Route("UploadRestaurantPicture")]
        public async Task<IActionResult> UploadRestaurantPicture(IFormFile avatar, Guid restaurantId)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    byte[] data;
                    using (MemoryStream stream = new MemoryStream())
                    {
                        await avatar.CopyToAsync(stream);
                        data = stream.ToArray();
                    }
                    if (await _sqlRepository.ChangeRestaurantPictureIfOwned(restaurantId, account.ID, data))
                    {
                        return RedirectToAction("MyRestaurant", new { id = restaurantId });
                    }
                }
            }
            catch
            {
            }
            return RedirectToAction("", new { FailedAvatarChange = true });
        }

        [HttpGet]
        [Route("MealEditor")]
        public async Task<IActionResult> MealEditor(Guid id, bool? addMealSuccess, bool? deleteMealSuccess, bool? changeImageSuccess, bool? deleteMealFailure, bool? addMealFailure)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    Restaurant restaurant = await _sqlRepository.FetchRestaurantIfOwned(account.ID, id, true);
                    if (restaurant != null)
                    {
                        ViewData["Account"] = account;
                        ViewData["Restaurant"] = restaurant;
                        ViewData["AddMealSuccess"] = (addMealSuccess.HasValue && addMealSuccess.Value);
                        ViewData["DeleteMealSuccess"] = (deleteMealSuccess.HasValue && deleteMealSuccess.Value);
                        ViewData["ChangeImageSuccess"] = (changeImageSuccess.HasValue && changeImageSuccess.Value);
                        ViewData["DeleteMealFailure"] = (deleteMealFailure.HasValue && deleteMealFailure.Value);
                        ViewData["AddMealFailure"] = (addMealFailure.HasValue && addMealFailure.Value);
                        return View();
                    }
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("AddMeal")]
        public async Task<IActionResult> AddMeal(AddMealModel amm)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    Restaurant restaurant = await _sqlRepository.FetchRestaurantIfOwned(account.ID, amm.RestaurantID);
                    if (restaurant != null)
                    {
                        ViewData["Account"] = account;
                        ViewData["Restaurant"] = restaurant;
                        ViewData["EditExisting"] = amm.EditExisting;
                        return View("AddMeal");
                    }
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpPost]
        [Route("SaveMeal")]
        public async Task<IActionResult> SaveMeal(AddMealModel amm)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    Restaurant restaurant = await _sqlRepository.FetchRestaurantIfOwned(account.ID, amm.RestaurantID);
                    if (restaurant != null)
                    {
                        if (ModelState.IsValid)
                        {
                            bool addMealSuccess = await _sqlRepository.AttemptAddEditMeal(amm);
                            return RedirectToAction("MealEditor", new { id = amm.RestaurantID, addMealSuccess = addMealSuccess, addMealFailure = !addMealSuccess });
                        }
                        ViewData["InvalidName"] = (ModelState["Name"].ValidationState != ModelValidationState.Valid);
                        ViewData["InvalidDescription"] = (ModelState["Description"].ValidationState != ModelValidationState.Valid);
                        ViewData["InvalidPrice"] = (ModelState["Price"].ValidationState != ModelValidationState.Valid);
                        ViewData["InvalidPrepTime"] = (ModelState["PrepTime"].ValidationState != ModelValidationState.Valid);
                        return await AddMeal(amm);
                    }
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpPost]
        [Route("UploadMealPicture")]
        public async Task<IActionResult> UploadMealPicture(IFormFile picture, Guid restId, Guid mealId)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    byte[] data;
                    using (MemoryStream stream = new MemoryStream())
                    {
                        await picture.CopyToAsync(stream);
                        data = stream.ToArray();
                    }
                    return RedirectToAction("MealEditor", new { id = restId, changeImageSuccess = await _sqlRepository.ChangeMealPictureIfOwned(restId, mealId, account.ID, data) });
                }
            }
            catch
            {
            }
            return RedirectToAction("", new { FailedAvatarChange = true });
        }

        [HttpGet]
        [Route("DeleteMeal")]
        public async Task<IActionResult> DeleteMeal(Guid restId, Guid mealId)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    bool deleteMealSuccess = await _sqlRepository.DeleteMealIfOwned(restId, mealId, account.ID);
                    return RedirectToAction("MealEditor", new { id = restId, deleteMealSuccess = deleteMealSuccess, deleteMealFailure = !deleteMealSuccess });
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("Cart")]
        public async Task<IActionResult> Cart(bool? checkoutSuccess)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    ViewData["CartContent"] = await _sqlRepository.GetCartContent(account.ID);
                    ViewData["CheckoutSuccess"] = checkoutSuccess.HasValue && checkoutSuccess.Value;
                    return View();
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("IncMeal")]
        public async Task<IActionResult> IncMeal(Guid tkItemId)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    await _sqlRepository.IncDecMeal(tkItemId, account.ID, true);
                    return RedirectToAction("Cart");
                }
            } catch { }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("DecMeal")]
        public async Task<IActionResult> DecMeal(Guid tkItemId)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    await _sqlRepository.IncDecMeal(tkItemId, account.ID, false);
                    return RedirectToAction("Cart");
                }
            }
            catch { }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("DelMeal")]
        public async Task<IActionResult> DelMeal(Guid tkItemId)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    await _sqlRepository.IncDecMeal(tkItemId, account.ID, false, true);
                    return RedirectToAction("Cart");
                }
            }
            catch { }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("Checkout")]
        public async Task<IActionResult> Checkout()
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    if ((await _sqlRepository.GetCartContent(account.ID)).Count() <= 0)
                    {
                        return RedirectToAction("", "");
                    }
                    return View(new CheckoutModel());
                }
            }
            catch { }
            return RedirectToAction("", "");
        }

        [HttpPost]
        [Route("Checkout")]
        public async Task<IActionResult> Checkout(CheckoutModel model)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    if (ModelState.IsValid)
                    {
                        if ((await _sqlRepository.GetCartContent(account.ID)).Count() <= 0)
                        {
                            return RedirectToAction("", "");
                        }
                        if (await _sqlRepository.TryCheckout(account.ID, model))
                        {
                            return RedirectToAction("Cart", new { checkoutSuccess = true });
                        }
                        else
                        {
                            ViewData["UnknownError"] = true;
                            return View(model);
                        }
                    }
                    ViewData["InvalidAddress"] = (ModelState["Address"].ValidationState != ModelValidationState.Valid);
                    ViewData["InvalidLocation"] = (ModelState["MapLatitude"].ValidationState != ModelValidationState.Valid) || (ModelState["MapLongitude"].ValidationState != ModelValidationState.Valid);
                    return View(model);
                }
            } catch { }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("TakeoutDetails")]
        public async Task<IActionResult> TakeoutDetails(Guid tkid)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    Takeout takeout = await _sqlRepository.SeeTakeoutIfAllowed(tkid, account.ID);
                    if (takeout != null)
                    {
                        ViewData["Takeout"] = takeout;
                        return View();
                    }
                }
            }
            catch { }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("ActiveTakeouts")]
        public async Task<IActionResult> ActiveTakeouts(Guid id)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    Restaurant restaurant = await _sqlRepository.FetchRestaurantIfOwned(account.ID, id);
                    if (restaurant != null)
                    {
                        ViewData["ID"] = restaurant.ID;
                        return View();
                    }
                }
            }
            catch { }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("MyOrders")]
        public async Task<IActionResult> MyOrders()
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    ViewData["ID"] = account.ID;
                    return View();
                }
            } catch { }
            return RedirectToAction("", "");
        }
    }
}
