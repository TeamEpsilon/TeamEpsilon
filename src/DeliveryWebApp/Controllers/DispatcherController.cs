﻿using DeliveryWebApp.Database;
using DeliveryWebApp.Database.Entries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DeliveryWebApp.Controllers
{
    [Route("Dispatcher")]
    public class DispatcherController : Controller
    {
        private ISqlRepository _sqlRepository;

        public DispatcherController(ISqlRepository sqlRepository)
        {
            _sqlRepository = sqlRepository;
        }

        [HttpGet]
        [Route("")]
        [Route("Index")]
        public async Task<IActionResult> Index()
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if ((account != null) || (account.AccountRole != AccountType.DispatcherUser))
                {
                    ViewData["DeliveryMen"] = await _sqlRepository.GetAvailableDeliveryBoys();
                    return View();
                }
            } catch { }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("OrderStation")]
        public async Task<IActionResult> OrderStation(string username)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if ((account != null) || (account.AccountRole != AccountType.DispatcherUser))
                {
                    ViewData["Username"] = username;
                    return View();
                }
            }
            catch { }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("AddPickup")]
        public async Task<IActionResult> AddPickup(string username, Guid tkid)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if ((account != null) || (account.AccountRole != AccountType.DispatcherUser))
                {
                    if (await _sqlRepository.AddPickupOrder(username, tkid))
                    {
                        return RedirectToAction("OrderStation", new { username = username });
                    }
                }
            } catch { }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("AddDelivery")]
        public async Task<IActionResult> AddDelivery(string username, Guid tkid)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if ((account != null) || (account.AccountRole != AccountType.DispatcherUser))
                {
                    if (await _sqlRepository.AddDropoffOrder(username, tkid))
                    {
                        return RedirectToAction("OrderStation", new { username = username });
                    }
                }
            }
            catch { }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("MapViewer")]
        public async Task<IActionResult> MapViewer(double lat, double lon)
        {
            try
            {
                ViewData["Lat"] = lat;
                ViewData["Long"] = lon;
                return View();
            }
            catch { }
            return RedirectToAction("", "");
        }
    }
}
