﻿using DeliveryWebApp.Database;
using DeliveryWebApp.Database.Entries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DeliveryWebApp.Controllers
{
    [Route("Delivery")]
    public class DeliveryController : Controller
    {
        private ISqlRepository _sqlRepository;

        public DeliveryController(ISqlRepository sqlRepository)
        {
            _sqlRepository = sqlRepository;
        }

        [HttpGet]
        [Route("")]
        [Route("Index")]
        public async Task<IActionResult> Index(bool? firstComplete)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if ((account != null) || (account.AccountRole != AccountType.DelivererUser))
                {
                    ViewData["Username"] = account.Username;
                    ViewData["FirstComplete"] = firstComplete.HasValue && firstComplete.Value;
                    return View();
                }
            }
            catch { }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("MarkAsComplete")]
        public async Task<IActionResult> MarkAsComplete()
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if ((account != null) || (account.AccountRole != AccountType.DelivererUser))
                {
                    return RedirectToAction("Index", "Delivery", new { firstComplete = await _sqlRepository.MarkOrderComplete(account.ID) });
                }
            } catch { }
            return RedirectToAction("", "");
        }
    }
}
