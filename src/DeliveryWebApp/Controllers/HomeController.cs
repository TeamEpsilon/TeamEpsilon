﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliveryWebApp.Database;
using DeliveryWebApp.Database.Entries;
using DeliveryWebApp.Models.Home;
using DeliveryWebApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryWebApp.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        private IReCaptcha _reCaptcha;
        private ISqlRepository _sqlRepository;

        public HomeController(IReCaptcha reCaptcha, ISqlRepository sqlRepository)
        {
            _reCaptcha = reCaptcha;
            _sqlRepository = sqlRepository;
        }

        [Route("")]
        public IActionResult Index(bool? completedDeletion, bool? completeEmailChange, bool? failEmailChange)
        {
            if (completedDeletion.HasValue)
            {
                ViewData["CompletedDeletion"] = completedDeletion.Value;
            }
            if (completeEmailChange.HasValue)
            {
                ViewData["CompleteEmailChange"] = completeEmailChange.Value;
            }
            if (failEmailChange.HasValue)
            {
                ViewData["FailEmailChange"] = failEmailChange.Value;
            }
            return View();
        }

        [Route("Rules")]
        public IActionResult Rules()
        {
            return View();
        }

        [Route("About")]
        public IActionResult About()
        {
            return View();
        }

        [Route("Register")]
        public IActionResult Register()
        {
            if (Request.ReadSimulatedAccessToken() != null)
            {
                return RedirectToAction("");
            }
            return View();
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (Request.ReadSimulatedAccessToken() != null)
            {
                return RedirectToAction("");
            }
            if ((ModelState.IsValid) && (await _reCaptcha.IsReCaptchaResponseValidAsync(model.ReCaptchaResponse)))
            {
                Account account = new Account(model.Username, model.Email, model.Password, model.FirstName, model.LastName, model.CellphoneNumber);
                if (await _sqlRepository.RegisterAccountAsync(account))
                {
                    return RedirectToAction("Confirm", new { Email = model.Email });
                }
            }
            ViewData["IsRegistrationFailed"] = true;
            return View(model);
        }

        [HttpGet]
        [Route("Confirm")]
        public async Task<IActionResult> Confirm(string email, string code)
        {
            if (Request.ReadSimulatedAccessToken() != null)
            {
                return RedirectToAction("");
            }
            if (email != null)
            {
                ViewData["Email"] = email;
                if (await _sqlRepository.CheckEmailAsync(email))
                {
                    return RedirectToAction("");
                }
                if (await _sqlRepository.IsAccountVerifiedAsync(email))
                {
                    ViewData["Verified"] = true;
                    return View();
                }
                if (code != null)
                {
                    if (await _sqlRepository.VerifyEmailAsync(email, code))
                    {
                        ViewData["Activated"] = true;
                    }
                    else
                    {
                        ViewData["Failed"] = true;
                    }
                    return View();
                }
                return View();
            }
            return RedirectToAction("");
        }

        [HttpGet]
        [Route("Resend")]
        public async Task<IActionResult> Resend(string email)
        {
            if ((email != null) && (await _sqlRepository.ResendVerificationEmailAsync(email)))
            {
                return Json(new { Success = true });
            }
            return Json(new { Success = false });
        }

        [Route("Login")]
        public IActionResult Login()
        {
            if (Request.ReadSimulatedAccessToken() != null)
            {
                return RedirectToAction("");
            }
            return View();
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (Request.ReadSimulatedAccessToken() != null)
            {
                return RedirectToAction("");
            }
            if ((ModelState.IsValid) && (await _reCaptcha.IsReCaptchaResponseValidAsync(model.ReCaptchaResponse)))
            {
                Account account = await _sqlRepository.GetAccountAsync(model.UsernameOrEmail);
                if ((account != null) && (account.CheckPassword(model.Password)))
                {
                    if (account.IsVerified) {
                        AccessToken accessToken = await _sqlRepository.GenerateAccessToken(account);
                        Response.WriteAccessToken(accessToken);
                        return RedirectToAction("");
                    }
                    else
                    {
                        return RedirectToAction("Confirm", new { Email = account.Email });
                    }
                }
            }
            ViewData["IsLoginFailed"] = true;
            return View(model);
        }

        [Route("Logout")]
        public async Task<IActionResult> Logout()
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account.AccountRole == AccountType.DelivererUser)
                {
                    // if user is a delivery user, clear all of his access tokens (so online activity is tracked better)
                    await _sqlRepository.PerformsCleanLogOut(account.ID);
                }
            } catch { }
            Response.ClearAccessToken();
            return RedirectToAction("");
        }

        [HttpGet]
        [Route("CheckUsername")]
        public async Task<IActionResult> CheckUsername(string username)
        {
            if (username == null)
            {
                return Json(new { Available = false });
            }
            return Json(new { Available = await _sqlRepository.CheckUsernameAsync(username) });
        }

        [HttpGet]
        [Route("CheckEmail")]
        public async Task<IActionResult> CheckEmail(string email)
        {
            if (email == null)
            {
                return Json(new { Available = false });
            }
            return Json(new { Available = await _sqlRepository.CheckEmailAsync(email) });
        }

        [HttpGet]
        [Route("User")]
        public async Task<IActionResult> User(string username)
        {
            try
            {
                Account myAccount = null;
                Account targetAccount = await _sqlRepository.GetAccountAsync(username);
                try
                {
                    AccessToken accessToken = Request.ReadSimulatedAccessToken();
                    myAccount = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                }
                catch
                {
                }
                if (targetAccount != null)
                {
                    ViewData["MyAccount"] = myAccount;
                    ViewData["TargetAccount"] = targetAccount;
                    return View();
                }
            }
            catch
            {
            }
            return RedirectToAction("");
        }

        [HttpGet]
        [Route("Restaurant")]
        public async Task<IActionResult> Restaurant(Guid id, bool? successAddToCart)
        {
            try
            {
                Restaurant restaurant = await _sqlRepository.FetchRestaurant(id, true);
                if (restaurant != null)
                {
                    Account account = null;
                    try
                    {
                        AccessToken accessToken = Request.ReadSimulatedAccessToken();
                        account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                    }
                    catch { }
                    ViewData["Account"] = account;
                    ViewData["Restaurant"] = restaurant;
                    ViewData["SuccessAddToCart"] = successAddToCart.HasValue && successAddToCart.Value;
                    return View();
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("MealDetails")]
        public async Task<IActionResult> MealDetails(Guid restId, Guid mealId)
        {
            try
            {
                Restaurant restaurant = await _sqlRepository.FetchRestaurant(restId, true);
                if (restaurant != null)
                {
                    Account account = null;
                    try
                    {
                        AccessToken accessToken = Request.ReadSimulatedAccessToken();
                        account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                    }
                    catch { }
                    ViewData["Account"] = account;
                    ViewData["Restaurant"] = restaurant;
                    ViewData["Meal"] = restaurant.Meals.Where(e => e.ID == mealId).First();
                    return View();
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }

        [HttpGet]
        [Route("AddToCart")]
        public async Task<IActionResult> AddToCart(Guid restId, Guid mealId)
        {
            try
            {
                AccessToken accessToken = Request.ReadSimulatedAccessToken();
                Account account = await _sqlRepository.FetchAccountByAccessToken(accessToken);
                if (account != null)
                {
                    return RedirectToAction("Restaurant", new { id = restId, successAddToCart = await _sqlRepository.AddMealToCart(restId, mealId, account) });
                }
            }
            catch
            {
            }
            return RedirectToAction("", "");
        }
    }
}
